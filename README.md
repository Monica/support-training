# Support Team Trainings

Welcome to the GitLab Support team trainings project

We use this space for:

- Support onboarding issues
- trainings / modules
- interview training

Pairing issue should be created in the [Support Pairing project](https://gitlab.com/gitlab-com/support/support-pairing/issues)

Any issues not related to the items listed should go in the [support issue tracker](https://gitlab.com/gitlab-com/support/support-team-meta/issues).

Support team resources:
 - Handbook: https://handbook.gitlab.com/handbook/support/
 - Support Workflows: https://handbook.gitlab.com/handbook/support/workflows/
 - GitLab docs: http://docs.gitlab.com/

For a list of available modules, please check the list of [issue templates](/.gitlab/issue_templates/).

## Guideline to Update Support Training module

There is no defined process to update the support training module. You may refer to the following workflow chart as a guideline to update Support Training module. 

```mermaid
graph TD;
  A[Is a typo or broken link] --> B[Self approve and merge];
  C[Is a new addition or change] --> D[Search for a maintainer];
  D --> H[Maintainer available?] --> E[Peer approve and merge];
  D --> F[Maintainer unavailable?] --> G[Search for an expert or manager] --> E;
```

If you are unsure about anything, feel free to ask on [#support_team-chat](https://gitlab.slack.com/archives/CCBJYEWAW), or reach out to your onboarding buddy, or your manager. 

