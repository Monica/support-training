

### [Promote Geo Secondary](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/runbooks/planned_failover_single_node.html#geo-planned-failover-for-a-single-node-configuration)



1. SSH into the **primary** site to stop and disable GitLab

```
sudo gitlab-ctl stop
```


2. Prevent GitLab from starting up again if the server unexpectedly reboots

```
sudo systemctl disable gitlab-runsvdir
```


* Note, that if you plan to[ update the primary domain DNS record](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/#step-4-optional-updating-the-primary-domain-dns-record), you may wish to lower the TTL now to speed up propagation.
3. Promote the secondary

On the secondary node, run


```
sudo gitlab-ctl geo promote
```




* Note that you can use `--force` to continue **without any further confirmation**.
* Also note that there is a [much more involved process for promotion](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/#promoting-a-secondary-site-running-on-a-single-node-running-gitlab-144-and-earlier) on GitLab version 14.4 and earlier.


### Repurpose Geo Primary as Secondary



1. [Remove the site from the UI](https://docs.gitlab.com/ee/administration/geo/replication/remove_geo_site.html)
* On the top bar, select **Menu > Admin**.
* On the left sidebar, select **Geo > Nodes**.
* Select the **Remove** button for the **secondary** site you want to remove.
* Confirm by selecting **Remove** when the prompt appears.
2. Stop and uninstall the **secondary** site from the node

```
 sudo gitlab-ctl stop
```



```
 # Stop gitlab and remove its supervision process
sudo gitlab-ctl uninstall
```


3. Drop the replication slot from the **primary** site’s database 

    Note that `gitlab-rails dbconsole` does not work for this because managing replication slots requires superuser permissions 


```
 sudo gitlab-psql
```


    1. Find the name of the relevant replication slot

     Note that this is the slot that is specified with `--slot-name` when running the replicate command: `gitlab-ctl replicate-geo-database`


```
 SELECT * FROM pg_replication_slots;
```


    2. Remove the replication slot for the **secondary** site \


```
 SELECT pg_drop_replication_slot('<name_of_slot>');
```


<h3 id="secondary-node">Secondary Node</h3>




1. Make sure Puma Sidekiq are off \


```
sudo gitlab-ctl stop puma
sudo gitlab-ctl stop sidekiq
```


2. [Check TCP connectivity](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html) to the **primary** site’s PostgreSQL server

```
gitlab-rake gitlab:tcp_check[10.128.20.4,5432]
```


3. Drop in the output from `cat ~gitlab-psql/data/server.crt` into `server.crt`
4. Import certificate \


```
install \
   -D \
   -o gitlab-psql \
   -g gitlab-psql \
   -m 0400 \
   -T server.crt ~gitlab-psql/.postgresql/root.crt
```


5. Test that the `gitlab-psql` user can connect to the **primary** site’s database

```
sudo \
   -u gitlab-psql /opt/gitlab/embedded/bin/psql \
   --list \
   -U gitlab_replicator \
   -d "dbname=gitlabhq_production sslmode=verify-ca" \
   -W \
   -h 10.128.20.4
```


6. Add the following to `/etc/gitlab/gitlab.rb`

```
roles(['geo_secondary_role'])

##
## Secondary address
## - replace '<secondary_site_ip>' with the public or VPC address of your Geo secondary site
##
postgresql['listen_address'] = '10.128.20.2'
postgresql['md5_auth_cidr_addresses'] = ['10.128.20.2/32']

##
## Database credentials password (defined previously in primary site)
## - replicate same values here as defined in primary site
##
postgresql['sql_user_password'] = 'f177150daae1cc4b26e7a00fff1ad7c4'
postgresql['sql_replication_password'] = 'f28c2759c80b00117170cca3a81a7df3'
gitlab_rails['db_password'] = 'abcd1234!'
```


7. Run `sudo gitlab-ctl reconfigure` to pick up rails changes
8. Run `sudo gitlab-ctl restart postgresql` to pick database changes
9. Initiate replication from the GEO secondary node

```
root@sr-env-68960e77-basic:~# gitlab-ctl replicate-geo-database \
>	--slot-name=2nd_site \
>	--host=10.128.20.4 \
>	--sslmode=verify-ca
No user created projects. Database not active

---------------------------------------------------------------
WARNING: Make sure this script is run from the secondary server
---------------------------------------------------------------

*** You are about to delete your local PostgreSQL database, and replicate the primary database. ***
*** The primary geo node is `10.128.20.4` ***

*** Are you sure you want to continue (replicate/no)? ***
Confirmation: replicate
Enter the password for gitlab_replicator@10.128.20.4:
* Executing GitLab backup task to prevent accidental data loss
* Stopping PostgreSQL and all GitLab services
* Checking for replication slot 2nd_site
* Creating replication slot 2nd_site
* Backing up postgresql.conf
* Moving old data directory to '/var/opt/gitlab/postgresql/data.1661860590'
* Starting base backup as the replicator user (gitlab_replicator)
pg_basebackup: initiating base backup, waiting for checkpoint to complete
pg_basebackup: checkpoint completed
pg_basebackup: write-ahead log start point: 0/6000028 on timeline 1
pg_basebackup: starting background WAL receiver
	0/92785 kB (0%), 0/1 tablespace (...lab/postgresql/data/backup_label)
18994/92785 kB (20%), 0/1 tablespace (...postgresql/data/base/16386/17710)
55775/92785 kB (60%), 0/1 tablespace (...postgresql/data/base/16386/21148)
92290/92785 kB (99%), 0/1 tablespace (...tlab/postgresql/data/global/4184)
92795/92795 kB (100%), 0/1 tablespace (...ostgresql/data/global/pg_control)
92795/92795 kB (100%), 1/1 tablespace                                    	 
pg_basebackup: write-ahead log end point: 0/6000100
pg_basebackup: waiting for background process to finish streaming ...
pg_basebackup: syncing data to disk ...
pg_basebackup: renaming backup_manifest.tmp to backup_manifest
pg_basebackup: base backup completed
* Restoring postgresql.conf
* Setting ownership permissions in PostgreSQL data directory
* Starting PostgreSQL and all GitLab services 
```


10. Resulting gitlab.rb file

The end result is an `/etc/gitlab/gitlab.rb` that looks like this:


```
external_url 'https://sr-env-68960e77-basic.env-68960e77.gcp.gitlabsandbox.net'

roles(['geo_secondary_role'])

postgresql['listen_address'] = '10.128.20.2'
postgresql['md5_auth_cidr_addresses'] = ['10.128.20.2/32']
postgresql['sql_user_password'] = 'f177150daae1cc4b26e7a00fff1ad7c4'
postgresql['sql_replication_password'] = 'f28c2759c80b00117170cca3a81a7df3'

gitlab_rails['db_password'] = 'abcd1234!'
```


<h3 id="configure-the-new-secondary-site">[Configure the New Secondary Site](https://docs.gitlab.com/ee/administration/geo/replication/configuration.html#configuring-a-new-secondary-site)</h3>


	



1. On the primary node, run `sudo cat /etc/gitlab/gitlab-secrets.json`
2. Place the contents of  `/etc/gitlab/gitlab-secrets.json` into a new file on the secondary node, also called `/etc/gitlab/gitlab-secrets.json`
3. Quickly verify that the copy was successful by running `md5sum /etc/gitlab/gitlab-secrets.json ` to ensure that hashed value is the same
4. Fix the permissions of the newly created file \


```
chown root:root /etc/gitlab/gitlab-secrets.json
chmod 0600 /etc/gitlab/gitlab-secrets.json
```


5. Reconfigure **your secondary** site for the change to take effect

```
gitlab-ctl reconfigure
gitlab-ctl restart
```



Note that if you have more than one node that is a part of your secondary site (i.e.: Rails, Sidekiq, Gitaly).

<h3 id="replicate-the-primary-site’s-ssh-host-keys-and-setup-fast-lookup">[Replicate the primary site’s SSH host keys and setup Fast Lookup](https://docs.gitlab.com/ee/administration/geo/replication/configuration.html#step-2-manually-replicate-the-primary-sites-ssh-host-keys)</h3>




1. On the secondary node(s), Make a backup of any existing SSH host keys \


```
find /etc/ssh -iname ssh_host_* -exec cp {} {}.backup.`date +%F` \;
```


2. Copy OpenSSH host keys from the **primary** site

```
scp root@sr-env-68960e77-omnibus.env-68960e77.gcp.gitlabsandbox.net:/etc/ssh/ssh_host_*_key* /etc/ssh
```


3. ensure the file permissions are correct for the newly copied keys on the **secondary**

```
chown root:root /etc/ssh/ssh_host_*_key*
chmod 0600 /etc/ssh/ssh_host_*_key
```


4. Verify fingerprint matches, on both primary and secondary nodes

```
for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done
```



```
root@sr-env-68960e77-omnibus:~# for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done
1024 SHA256:3mFJHHL9A239nQkzzZtioWFEQKfgb+sW28llPyBTUQQ root@sr-env-68960e77-omnibus (DSA)
256 SHA256:D/WMtJLrx02XXlvU7Z7zTgwniJtDXyZoxP4WdP5s56Y root@sr-env-68960e77-omnibus (ECDSA)
256 SHA256:HRGYMP8Q7ytPFthWu9XDaRY9zLNzEyMk/xqgPh9hrh0 root@sr-env-68960e77-omnibus (ED25519)
3072 SHA256:Qrw6EZ+domRgtOCJUPnshYKhz1qwD3T+VCit1cNgH5A root@sr-env-68960e77-omnibus (RSA)
```



```
root@sr-env-68960e77-basic:~# for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done
1024 SHA256:3mFJHHL9A239nQkzzZtioWFEQKfgb+sW28llPyBTUQQ root@sr-env-68960e77-omnibus (DSA)
256 SHA256:D/WMtJLrx02XXlvU7Z7zTgwniJtDXyZoxP4WdP5s56Y root@sr-env-68960e77-omnibus (ECDSA)
256 SHA256:HRGYMP8Q7ytPFthWu9XDaRY9zLNzEyMk/xqgPh9hrh0 root@sr-env-68960e77-omnibus (ED25519)
3072 SHA256:Qrw6EZ+domRgtOCJUPnshYKhz1qwD3T+VCit1cNgH5A root@sr-env-68960e77-omnibus (RSA)
```


5. Now let’s verify the correct public keys for the existing private keys:

```
# This will print the fingerprint for private keys:
for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done

# This will print the fingerprint for public keys:
for file in /etc/ssh/ssh_host_*_key.pub; do ssh-keygen -lf $file; done
```



```
root@sr-env-68960e77-omnibus:~# for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done
1024 SHA256:3mFJHHL9A239nQkzzZtioWFEQKfgb+sW28llPyBTUQQ root@sr-env-68960e77-omnibus (DSA)
256 SHA256:D/WMtJLrx02XXlvU7Z7zTgwniJtDXyZoxP4WdP5s56Y root@sr-env-68960e77-omnibus (ECDSA)
256 SHA256:HRGYMP8Q7ytPFthWu9XDaRY9zLNzEyMk/xqgPh9hrh0 root@sr-env-68960e77-omnibus (ED25519)
3072 SHA256:Qrw6EZ+domRgtOCJUPnshYKhz1qwD3T+VCit1cNgH5A root@sr-env-68960e77-omnibus (RSA)
root@sr-env-68960e77-omnibus:~# for file in /etc/ssh/ssh_host_*_key.pub; do ssh-keygen -lf $file; done
1024 SHA256:3mFJHHL9A239nQkzzZtioWFEQKfgb+sW28llPyBTUQQ root@sr-env-68960e77-omnibus (DSA)
256 SHA256:D/WMtJLrx02XXlvU7Z7zTgwniJtDXyZoxP4WdP5s56Y root@sr-env-68960e77-omnibus (ECDSA)
256 SHA256:HRGYMP8Q7ytPFthWu9XDaRY9zLNzEyMk/xqgPh9hrh0 root@sr-env-68960e77-omnibus (ED25519)
3072 SHA256:Qrw6EZ+domRgtOCJUPnshYKhz1qwD3T+VCit1cNgH5A root@sr-env-68960e77-omnibus (RSA)
```

```
root@sr-env-68960e77-basic:~# for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done
1024 SHA256:3mFJHHL9A239nQkzzZtioWFEQKfgb+sW28llPyBTUQQ root@sr-env-68960e77-omnibus (DSA)
256 SHA256:D/WMtJLrx02XXlvU7Z7zTgwniJtDXyZoxP4WdP5s56Y root@sr-env-68960e77-omnibus (ECDSA)
256 SHA256:HRGYMP8Q7ytPFthWu9XDaRY9zLNzEyMk/xqgPh9hrh0 root@sr-env-68960e77-omnibus (ED25519)
3072 SHA256:Qrw6EZ+domRgtOCJUPnshYKhz1qwD3T+VCit1cNgH5A root@sr-env-68960e77-omnibus (RSA)
root@sr-env-68960e77-basic:~# for file in /etc/ssh/ssh_host_*_key.pub; do ssh-keygen -lf $file; done
1024 SHA256:3mFJHHL9A239nQkzzZtioWFEQKfgb+sW28llPyBTUQQ root@sr-env-68960e77-omnibus (DSA)
256 SHA256:D/WMtJLrx02XXlvU7Z7zTgwniJtDXyZoxP4WdP5s56Y root@sr-env-68960e77-omnibus (ECDSA)
256 SHA256:HRGYMP8Q7ytPFthWu9XDaRY9zLNzEyMk/xqgPh9hrh0 root@sr-env-68960e77-omnibus (ED25519)
3072 SHA256:Qrw6EZ+domRgtOCJUPnshYKhz1qwD3T+VCit1cNgH5A root@sr-env-68960e77-omnibus (RSA)
```


6. Add the following to your `sshd_config` file on both nodes to enable Fast Lookup

    [Fast Lookup is required for Geo](https://docs.gitlab.com/ee/administration/operations/fast_ssh_key_lookup.html#fast-lookup-is-required-for-geo). This file is usually located at `/etc/ssh/sshd_config`:


```
Match User git    # Apply the AuthorizedKeysCommands to the git user only
  AuthorizedKeysCommand /opt/gitlab/embedded/service/gitlab-shell/bin/gitlab-shell-authorized-keys-check git %u %k
  AuthorizedKeysCommandUser git
Match all    # End match, settings apply to all users again
```


7. Restart sshd on **on both** site node(s)

```
sudo service ssh reload
```


8. On the secondary site node(s). Edit `/etc/gitlab/gitlab.rb` and add the side name. This is unique.

```
gitlab_rails['geo_node_name'] = 'node2'
```


9. Resulting gitlab.rb file

The end result is an `/etc/gitlab/gitlab.rb` that looks like this:


```
external_url 'https://sr-env-68960e77-basic.env-68960e77.gcp.gitlabsandbox.net'

roles(['geo_secondary_role'])

postgresql['listen_address'] = '10.128.20.2'
postgresql['md5_auth_cidr_addresses'] = ['10.128.20.2/32']
postgresql['sql_user_password'] = 'f177150daae1cc4b26e7a00fff1ad7c4'
postgresql['sql_replication_password'] = 'f28c2759c80b00117170cca3a81a7df3'

gitlab_rails['db_password'] = 'abcd1234!'
gitlab_rails['geo_node_name'] = 'node2'
```




10. Reconfigure the **Rails node(s) on your secondary** site for the change to take effect:

```
sudo gitlab-ctl reconfigure
```


11.  Navigate to the Primary Node GitLab Instance
    * 
* On the top bar, select **Menu > Admin**.
* On the left sidebar, select **Geo > Sites**.
* Select **Add site**.

    

* Fill in **Name** with the gitlab_rails['geo_node_name'] in /etc/gitlab/gitlab.rb. These values must always match _exactly_, character for character.
* Fill in **URL** with the external_url in /etc/gitlab/gitlab.rb. These values must always match, but it doesn’t matter if one ends with a / and the other doesn’t.
* (Optional) Choose which groups or storage shards should be replicated by the **secondary** site. Leave blank to replicate all. Read more in[ selective synchronization](https://docs.gitlab.com/ee/administration/geo/replication/configuration.html#selective-synchronization).
*  Select **Save changes** to add the **secondary** site.
12. Disable writes to the authorized_keys file
* On the top bar, select **Menu > Admin**.
* On the left sidebar, select **Settings > Network**.
* Expand **Performance optimization**.
* Clear the **Use authorized_keys file to authenticate SSH keys** checkbox.
* Select **Save changes**.





13. Restart GitLab on Secondary node(s)

```
sudo gitlab-ctl restart
```


14. Check Geo on both nodes

```
gitlab-rake gitlab:geo:check
```



```
root@sr-env-68960e77-omnibus:~# gitlab-rake gitlab:geo:check
Checking Geo ...

GitLab Geo is available ...
GitLab Geo is enabled ... yes
This machine's Geo node name matches a database record ... yes, found a primary node named "node1"
HTTP/HTTPS repository cloning is enabled ... yes
Machine clock is synchronized ... yes
Git user has default SSH configuration? ... yes
OpenSSH configured to use AuthorizedKeysCommand ... yes
GitLab configured to disable writing to authorized_keys file ... yes
GitLab configured to store new projects in hashed storage? ... yes
All projects are in hashed storage? ... yes

Checking Geo ... Finished
```



```
root@sr-env-68960e77-basic:~# gitlab-rake gitlab:geo:check
Checking Geo ...

GitLab Geo secondary database is correctly configured ... yes
Database replication enabled? ... yes
Database replication working? ... yes
GitLab Geo HTTP(S) connectivity ...
* Can connect to the primary node ... yes
GitLab Geo is available ...
GitLab Geo is enabled ... yes
This machine's Geo node name matches a database record ... yes, found a secondary node named "node2"
HTTP/HTTPS repository cloning is enabled ... yes
Machine clock is synchronized ... yes
Git user has default SSH configuration? ... yes
OpenSSH configured to use AuthorizedKeysCommand ... yes
GitLab configured to disable writing to authorized_keys file ... yes
GitLab configured to store new projects in hashed storage? ... yes
All projects are in hashed storage? ... yes

Checking Geo ... Finished
```



<h2 id="troubleshooting-maintenance">Troubleshooting/Maintenance</h2>




1. [Starting/Pausing Replication ](https://docs.gitlab.com/ee/administration/geo/index.html#pausing-and-resuming-replication)

```
#from secondary
gitlab-ctl geo-replication-pause

#from secondary
gitlab-ctl geo-replication-resume
```


2. [Reset Secondary Node Replication](https://docs.gitlab.com/ee/administration/geo/replication/troubleshooting.html#resetting-geo-secondary-node-replication) 
* From the secondary node stop Sidkiq and Geo Logcursor

```
gitlab-ctl status sidekiq
# run: sidekiq: (pid 10180) <- this is the PID you will use
kill -TSTP 10180 # change to the correct PID

gitlab-ctl stop sidekiq
gitlab-ctl stop geo-logcursor
```


* watch the[ Sidekiq logs](https://docs.gitlab.com/ee/administration/logs/index.html#sidekiq-logs) to know when Sidekiq jobs processing has finished

```
gitlab-ctl tail sidekiq
```


* Rename repository storage folders and create new ones

```
mv /var/opt/gitlab/git-data/repositories /var/opt/gitlab/git-data/repositories.old
mkdir -p /var/opt/gitlab/git-data/repositories
chown git:git /var/opt/gitlab/git-data/repositories
```


* Rename other data folders and create new ones

```
gitlab-ctl stop

mv /var/opt/gitlab/gitlab-rails/shared /var/opt/gitlab/gitlab-rails/shared.old
mkdir -p /var/opt/gitlab/gitlab-rails/shared

mv /var/opt/gitlab/gitlab-rails/uploads /var/opt/gitlab/gitlab-rails/uploads.old
mkdir -p /var/opt/gitlab/gitlab-rails/uploads

gitlab-ctl start postgresql
gitlab-ctl start geo-postgresql
```


* Reconfigure to recreate the folders and make sure permissions and ownership are correct

```
sudo gitlab-ctl reconfigure
```


* Reset the Tracking Database

```
gitlab-rake db:drop:geo DISABLE_DATABASE_ENVIRONMENT_CHECK=1   # on a secondary app node
gitlab-ctl reconfigure     # on the tracking database node
gitlab-rake db:migrate:geo # on a secondary app node
```


* Make sure all services are started

```
sudo gitlab-ctl start
```


3. General secondary replication problem.

[Follow the replication troubleshooting documentation](https://docs.gitlab.com/ee/administration/geo/replication/troubleshooting.html)

