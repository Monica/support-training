# A GitLab Support engineers guide to Vault by Hashicorp

[TOC]

## Introduction

This training aims to cover the setup of [Vault](https://www.hashicorp.com/products/vault) from the point of view of GitLab CI communicating with it to obtain secrets.

It will not cover the safe and secure setup of Vault itself as that is a large topic external to CI.

We will be using Vault in [dev-server](https://developer.hashicorp.com/vault/docs/concepts/dev-server) mode which has some key restrictions for production use but not for the sake of training or testing:

  1. Vault operates entirely in memory so when the process is terminated everything is lost
  1. There is no HTTPS/TLS enabled

The details in this training do not change as a result of running in `dev-server` mode as everything else is the same.

A video based version of this training is available on [GitLab Unfiltered](https://www.youtube.com/watch?v=iFVJDeJ0_N0)

## Prerequisites

  1. Before starting you will need a working GitLab instance which is accessible via HTTPS with a valid certificate. HTTPS is required for ID Tokens
  1. Somewhere to run Vault. The default port is `8200` and we will be using that throughout. Your runner(s) need to be able to communicate with Vault to obtain secrets
  1. A project on your instance to run the CI in. Make a note of the project ID now, I have used `26`
  1. A Premium or higher license on your test instance/project with a working runner

## Setting up Vault and creating some secrets

### Downloading and extracting Vault
You can find the source code and source code releases [on the Vault GitHub](https://github.com/hashicorp/vault/releases).

There are precompiled binaries [on their release server](https://releases.hashicorp.com/vault/)

At the time of writing [1.13.2](https://releases.hashicorp.com/vault/1.13.2/) was the most recent so lets grab that, I will be using the `amd64` version. If you are using a different architecture you will need to adjust the commands (if you are unsure what this means you likely want the `amd64` version too):

```bash
$ mkdir ~/vault
$ cd ~/vault
$ curl https://releases.hashicorp.com/vault/1.13.2/vault_1.13.2_linux_amd64.zip -o vault_1.13.2_linux_amd64.zip
```

You should always verify your downloads are complete, even more so with something of this nature if you were running it in production:

```bash
$ curl https://releases.hashicorp.com/vault/1.13.2/vault_1.13.2_SHA256SUMS -o vault_1.13.2_SHA256SUMS
$ sha256sum --ignore-missing -c vault_1.13.2_SHA256SUMS
vault_1.13.2_linux_amd64.zip: OK
```

If you see something other than "OK" at the end of this line you should download the file(s) again

These official releases do not contain any kind of installer. Some distributions do have have packages for Vault and you can opt to use this if you want however I would advise not setting it to start at boot time unless you plan to maintain it. For the sake of the training we can run a one off process which we can terminate when we're done or simply reboot to clean up.

Lets extract the file:

```bash
$ unzip vault_1.13.2_linux_amd64.zip
Archive:  vault_1.13.2_linux_amd64.zip
 inflating: vault
```

As mentioned there's no installer or anything fancy here, just the `vault` binary. For me it was already set executable but just to be sure let's set that:

```bash
$ chmod 755 vault
```

To make our lives a bit simpler lets move `vault` in to `/usr/local/bin` which should be in your `$PATH`:

```bash
sudo chown root:root vault
sudo mv vault /usr/local/bin
```

Running `vault` with no parameters now should give you the usage instructions, if not head back a bit and try to work out why:

```bash
$ vault
Usage: vault <command> [args]

<snip>
```

### Running vault

As mentioned earlier we will be running vault in developer mode for simplicities sake:

```bash
$ vault server -dev -dev-listen-address "0.0.0.0:8200" -dev-kv-v1 
```

Lets break this down a bit

  * `vault` - the binary we just installed
  * `-dev` - tell vault to start in developer mode
  * `-dev-listen-address "0.0.0.0:8200"` - tell vault to listen on all IPv4 addresses on port 8200, you can modify this if you want to. By default in developer mode Vault listens on `127.0.0.1` which is local only
  * `-dev-kv-v1` - Vault has two internal formats for storing secrets, customers with legacy setups will likely be running version 1 so lets emulate that as by default developer mode will run in v2

You will get quite a lot of console output when you run this and you will note that vault is not sent to the background. You can append ` &` to the end of the command to background it or start another shell session to run the future command we need to. I would advise simply starting a new shell as you can then hit [ctrl+c] in the shell you ran `vault` in to clean up at the end.

Key bits of information to check are:

```
Listener 1: tcp (addr: "0.0.0.0:8200", cluster address: "0.0.0.0:8201", max_request_duration: "1m30s", max_request_size: "33554432", tls: "disabled")
```

Make sure we're listening on `addr: "0.0.0.0:8200"` (or the port you have chosen instead) and that `tls: "disabled"` is set

Towards the end of this output (in yellow if your terminal mapping is the same as mine) you will see

```bash
You may need to set the following environment variables:

    $ export VAULT_ADDR='http://0.0.0.0:8200'
```

If you changed the port it will be different, however this is important as by default the `vault` binary will use HTTPS to talk to the server on the default port so we won't be able to do any configuration. `vault` will tell you about this with the following error if you are using the default port:

```
http: server gave HTTP response to HTTPS client
```

If you have changed port you will get a time out error.

#### Alternative methods for running Vault

You can also choose another method to run Vault. As mentioned some distributions provide packages and there is also a Docker container.

This guide assumes you are running version 1 of the Vault keystore, you can check the [Which Version is my Vault KV Mount?](https://support.hashicorp.com/hc/en-us/articles/4404288741139-Which-Version-is-my-Vault-KV-Mount-) article on HashiCorp's support portal to check your Vault server is running.

If your instance test instance is running version 2 you can *delete* the `secret` path and recreate it as version 1. This is a descrtuctive operation so if you have secrets stored already that you need be aware ([source](https://github.com/hashicorp/docker-vault/issues/111)):

```bash
$ vault secrets disable secret
$ vault secrets enable -version=1 -path=secret -description=some_description kv
```



### Adding some secrets

These first sections are rehashed from [Authenticating and reading secrets with HashiCorp Vault](https://docs.gitlab.com/15.10/ee/ci/examples/authenticating-with-hashicorp-vault/) but hopefully include more information to help your understanding.

Start a new shell on the same server and make sure you run the correct `export VAULT_ADDR...` for your setup. Remember that if your shell ends or you start a new shell for any reason you will have to run this again:

```bash
$ export VAULT_ADDR='http://0.0.0.0:8200'
```

Lets add some secrets:

```bash
$ vault kv put secret/vault-training/staging/db password=supersecretstagingpassword
$ vault kv put secret/vault-training/production/db password=supersecretproductionpassword
```

Every time you run a `vault` command it will return either `Success! <further details here>` or some form of error. In the event of an error the descriptions are not always helpful depending on what happened but in my experience so far it's usually a typo.

We can break these commands down as:

  - `vault` - the `vault` command
  - `kv` - tell `vault` - we're performing a command against the Key-Vault storage
  - `put` - we want to write a secret to the vault
  - `secret/some/path/to/write` the path of the secret.
    - In `kv-v1` all secrets should be written to `secret/*`
  - `secretname=secret` - the `field` (in this case `secretname`) and `secret` we wish to write to that field

With Vault's version 1 key store all secrets have to live in the `secret/` path however after this you can specify any path you wish in order to organize your secrets. In this instance my test project is called `vault-training` so I have chosen that as a base, along with `staging/production` and then `db` as an example of a service you may require a password for.

You can verify these (or any secrets) with the `get` command:

```bash
$ vault kv get -field=password secret/vault-training/staging/db
supersecretstagingpassword
$ vault kv get -field=password secret/vault-training/production/db
supersecretproductionpassword
```

We can break these commands down as:

  - `vault` - the `vault` command
  - `kv` - tell `vault` - we're performing a command against the Key-Vault storage
  - `get` - we want to read a secret
  - `-field=name` - the `field` name of the secret
  - `secret/some/path/to/read` the path of the secret

The ordering of this command is important and you cannot place `-field=name` at the end.

All look the same? Great let's move on!

## Configuring security policies in Vault

There are two layers at play when it comes to securing secrets in Vault

  - [Policies](https://developer.hashicorp.com/vault/docs/concepts/policies) which map secret paths to `capabilities`, such as `read` or `create`
  - [Authentication](https://developer.hashicorp.com/vault/docs/concepts/auth) which maps client logins to `Policies` to define what that client can do
    - client in this instance can mean anything connecting to `vault` be it a CI script, some other automation/script, or even a user

### Configuring policies

Lets go ahead and configure two `read` only policies, one for each secret we defined earlier:

```bash
$ vault policy write vault-training-staging - <<EOF
# Policy name: vault-training-staging
#
# Read-only permission on 'secret/vault-training/staging/*' path
path "secret/vault-training/staging/*" {
  capabilities = [ "read" ]
}
EOF
```

```bash
$ vault policy write vault-training-production - <<EOF
# Policy name: vault-training-production
#
# Read-only permission on 'secret/vault-training/production/*' path
path "secret/vault-training/production/*" {
  capabilities = [ "read" ]
}
EOF
```

If you are not familiar with the `<<EOF` syntax this is known as [Here documentation](https://en.wikipedia.org/wiki/Here_document#Unix_shells) or often simply `heredoc` for short and it allows you to enter multi line shell commands which will not be executed until the end `EOF` is seen. These commands can be copied/pasted in to your shell or you can type them out if wish.

`vault` does allow you to specify a file rather than using this syntax but for the sake of simplicity in this situation we will be using this quite a bit.

Breaking down the commands we have just run we have:

  - `vault` - the `vault` command
  - `policy` - tell vault we want to perform a `policy` command
  - `write` - we are looking to write a policy 
  - `<policy-name>` - in our case `vault-training-staging` and `vault-training-production`
  - `-` this tells `vault` to expect the policy to come via standard input on the command line. If you wanted to use a file here you would specify the `filename` rather than `-` and exclude the next part
  - `<<EOF` start the heredoc block
  - Then we have the policy (including some descriptive comments)
  - `EOF` - finally the `EOF` which triggers your shell to send the full command

You can verify a policy with:

```bash
vault policy read <policy-name>
```

For example:

```bash
$ vault policy read vault-training-staging
# Policy name: vault-training-staging
#
# Read-only permission on 'secret/vault-training/staging/*' path
path "secret/vault-training/staging/*" {
  capabilities = [ "read" ]
}
```

Note that the comments we entered are preserved, very useful on larger vaults to track change controls/ticket IDs etc.

[Policy Syntax](https://developer.hashicorp.com/vault/docs/concepts/policies#policy-syntax) is covered in detail in the documentation but for a basic read only policy you need:

```bash
path "secret/path/to/secret/*" {
  capabilities = [ "read" ]
}
```

The value in quotes after `path` is the same as the one you used `vault kv put` to add your secrets.

### Configuring JWT authentication

[JWT](https://jwt.io/) is an open industry standard method for representing claims securely between two parties. It is used in several places in GitLab but we will be focusing on their use with ID Tokens and CI here.

You should **never** share a JWT with anyone, whilst they are often encrypted they can contain secrets and in certain situations be used in replay attacks to obtain information.

First we need to enable `jwt` authentication as that is how GitLab will authenticate to Vault

```bash
vault auth enable jwt
```

Then we need to tell Vault to accept requests signed by our instance [via JWT](https://developer.hashicorp.com/vault/docs/auth/jwt#jwt-authentication)

```bash
vault write auth/jwt/config jwks_url="https://gitlab.example.com/-/jwks" bound_issuer="gitlab.example.com"
```

Here we are writing to `auth` provider telling it to configure `jwt` with the following parameters:
  - `jwks_url="https://gitlab.example.com/-/jwks"` - This must be the HTTPS url for your instance with `/-/jwks` appended and Vault must be able to connect to obtain the details it needs to be able to verify requests that come from the instance later on in CI.
  - `bound_issuer="gitlab.example.com"` - This is used as part of the validation for requests from CI and is also going to be our stumbling block later.

### Configuring authentication roles

Authenticating clients include the `role` they wish to access as they connect along with lots of other information.

Lets go ahead and configure two roles, one for each of the policies we defined earlier:

```bash
$ vault write auth/jwt/role/vault-training-staging - <<EOF
{
  "role_type": "jwt",
  "policies": ["vault-training-staging"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims": {
    "project_id": "26",
    "ref": "main",
    "ref_type": "branch"
  }
}
EOF
```

```bash
$ vault write auth/jwt/role/vault-training-production - <<EOF
{
  "role_type": "jwt",
  "policies": ["vault-training-production"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims_type": "glob",
  "bound_claims": {
    "project_id": "26",
    "ref_protected": "true",
    "ref_type": "branch",
    "ref": "auto-deploy-*"
  }
}
EOF
```

Some of this may be starting to look familiar to you now. We've run the `vault` command to `write` some information. In the first case we're writing to the roles path `auth/jwt/role` to a role called `vault-training-staging` and `vault-training-production`.

Lets take a closer look at the actual JSON:

 - `role_type` - this will always be `jwt` for us as we're using JWT to authenticate
 - `policies` - a list of policies that clients connecting against this role will be able to use
 - `token_explicit_max_ttl` - how long the token that is returned to the client is valid for if they match this role (seconds)
 - `user_claim` - which field sent by the client to use as a unique identifier for this session
 - `bound_claims_type` - can be `string` (default) or `glob`, used to control how to parse strings in `bound_claims`
 - `bound_claims` - which values sent in the JWT from the client have to match to be authenticated against this role

If you need to modify an existing role you can run the command again with an updated JSON object and it will be overwritten.

When GitLab authenticates to vault it sends a JWT with lots of different information in it. You can review this in [Authenticating and reading secrets with HashiCorp Vault - How it works](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/#how-it-works). You can specify any number of these in `bound_claims` to limit access to different secrets.

In our examples above for `vault-training-staging` we are allowing any connection from any CI job running in `project_id` 26 (which we mentioned at the start) and in branch `main`.

The `vault-training-production` example gets a bit more complicated because we enable `glob` based matching with `bound_claims_type` and use a `ref` of `auto-deploy-*` so we can use any branch name that starts with `auto-deploy-` to access this secret. However we also specify `ref_protected` to be `true` which means the branch also must be configured as a protected branch in the project. You can do this with [wildcard rules](https://docs.gitlab.com/ee/user/project/protected_branches.html#protect-multiple-branches-with-wildcard-rules) which you should go and setup now before proceeding.

If you are running a version of GitLab prior to 16 then while you are in your GitLab instance setting up the protected branch verify that `Limit JSON Web Token (JWT) access` is disabled for now under `Settings > CI/CD > Token Access` for your test project as we need it off for our first example.

From GitLab 16 onwards this setting is automatic based on the presence of `id_tokens:` in your job definition. See the [deprecation notice](https://docs.gitlab.com/ee/update/deprecations.html#hashicorp-vault-integration-will-no-longer-use-ci_job_jwt-by-default) for further details.

The training was authored around a pre 16 instance as I suspect most customers looking to perform this conversion will be there.

## Retrieving secrets in CI

### Using CI_JOB_JWT or CI_JOB_JWT_V1 to obtain secrets

A lot of customers will be using this method as it's been in GitLab for a while and so far it's what we have been working towards so we can understand what we need to change afterwards.

In your test project create a `.gitlab-ci.yml` in the `main` branch with the following. You will need to update `export VAULT_ADDR=http://10.138.0.2:8200` in the `script:` section to the address for your Vault server that is accessible from your runner.

```yaml
CI_JOB_JWT:
  image: hashicorp/vault:latest
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /^auto-deploy-*/ # If the branch name starts with `auto-deploy-*` use the production role
      variables:
        VAULT_AUTH_ROLE: 'vault-training-production'
    - if: $CI_COMMIT_REF_NAME # Otherwise use the staging role
      variables:
        VAULT_AUTH_ROLE: 'vault-training-staging'

  script:
    # This is your Vault server address
    - export VAULT_ADDR=http://10.138.0.2:8200

    # Check what role we're using
    - echo $VAULT_AUTH_ROLE

    # Authenticate to the vault server
    - export VAULT_TOKEN="$(vault write -field=token auth/jwt/login role=$VAULT_AUTH_ROLE jwt=$CI_JOB_JWT)"

    # Attempt to get the staging password - only works on `main`
    - export PASSWORD="$(vault kv get -field=password secret/vault-training/staging/db)"
    - echo $PASSWORD

    # Attempt to get the production password - only works on branches starting `auto-deploy-`
    - export PASSWORD="$(vault kv get -field=password secret/vault-training/production/db)"
    - echo $PASSWORD
```

This example is a little bit more complex that the examples in the documentation we're following along with but it means you can run the same CI against both `main` and one of the protected branches `auto-deploy-*` without editing it.

In the `rules:` section we define `VAULT_AUTH_ROLE` depending on the branch. The contents of this variable match up with role names we setup in "Configuring authentication roles" earlier.

Commit this change to `main` and check the pipeline to see what happened.

Create a new branch based off `main` called `auto-deploy-1` and check the pipeline for that new branch to see what happened.

Hopefully you will see that in `main` only the staging password was accessible, and in your `auto-deploy-1` branch only production was accessible.

Congratulations you've successfully configured Vault and GitLab CI to secure your secrets.

## Reconfiguring Vault to allow the new ID Tokens feature

We are now traveling beyond [Authenticating and reading secrets with HashiCorp Vault](https://docs.gitlab.com/15.10/ee/ci/examples/authenticating-with-hashicorp-vault/).

At this point we have a basic Vault/CI setup which will be similar in nature to a lot of customers who are looking at the deprecation notice and trying to ensure their environment continues to run after they upgrade to 16.5.

You may recall when we configured JWT in Vault earlier I mentioned a sticking point, this is it.

Part of the [JWT specification includes an `iss` (short for Issuer) field](https://www.rfc-editor.org/rfc/rfc7519#section-4.1.1) which is defined by the client but it _must_ match what we specified in `bound_issuer` when we enabled JWT authentication.

For the `CI_JOB_JWT` JWT object we only specified the domain name for the GitLab instance without the `https://` prefix but in the new tokens we send the full URL.

This means people looking to migrate will try to update their `bound_issuer` and then run in to problems.

Update yours with:

```bash
vault write auth/jwt/config jwks_url="https://gitlab.example.com/-/jwks" bound_issuer="https://gitlab.example.com"
```

then go back to your project and run the CI again in `main` and your branch.

There will be a lot of errors there but the first one is the important one because that's where the authentication happens.

```
error validating token: invalid issuer (iss) claim
```

As we have changed our `bound_issuer` all of our CI is now broken. For large organizations with lots of CI and secrets this is quite a painful thing to run in to!

Quick, change it back and check the jobs work again.

### Configuring Vault with a second authentication path

We can configure Vault to add a second authentication path using the new method. 

This will allow us to migrate to the new ID Tokens feature on a project by project basis to avoid any major outages.

The most granular we can go is on a project basis as we have to enable  `Limit JSON Web Token (JWT) access` under `Settings > CI/CD > Token Access` in a project to enable ID Tokens. This in turn disables the `CI_JOB_JWT` token.

If we take a deep dive in to the Vault documentation [on authentication](https://developer.hashicorp.com/vault/docs/auth) we can see mention of a `-path` parameter. Lets use that to enable JWT on another path so we don't impact the existing one:

```bash
$ vault auth enable -path jwt_v2 jwt
```

You may recognize this from the "Configuring JWT authentication" earlier just with the addition of `-path jwt_v2` (the ordering here is important, you cannot place `-path jwt_v2` at the end of the command). 

By default `-path` is set to the same as the authentication method, in our case earlier this was `jwt`. This is why in our previous commands for `auth` and `role` were `write`s to `auth/jwt/<something>`. Now we have added a `jwt_v2` path for `jwt` authentication we can run commands specifically against that path:

```bash
$ vault write auth/jwt_v2/config jwks_url="https://gitlab.example.com/-/jwks" bound_issuer="https://gitlab.example.com"
```

You can go and run your CI again now and it will still work as long as `Limit JSON Web Token (JWT) access` is disabled, great news!

### Reconfiguring our authentication roles

One downside to this method is our previously created roles are bound to the default `jwt` path so we will have to recreate them.

It is highly likely this step could be automated but given the risks involved it's not something I have looked in to nor would I suggest anyone try at scale as any mistakes could be a disaster.

The two commands are basically the same except for the `write` path:

```bash
$ vault write auth/jwt_v2/role/vault-training-staging - <<EOF
{
  "role_type": "jwt",
  "policies": ["vault-training-staging"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims": {
    "project_id": "26",
    "ref": "main",
    "ref_type": "branch"
  }
}
EOF
```

```bash
$ vault write auth/jwt_v2/role/vault-training-production - <<EOF
{
  "role_type": "jwt",
  "policies": ["vault-training-production"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims_type": "glob",
  "bound_claims": {
    "project_id": "26",
    "ref_protected": "true",
    "ref_type": "branch",
    "ref": "auto-deploy-*"
  }
}
EOF
```

We have changed the `vault write` path from `auth/jwt/...` to `auth/jwt_v2/...` so that these roles are written to this new authentication path.

As the policies we created earlier do not reference an authentication type they remain unchanged and can be referenced as they were before. Given policies are the glue between authentication and secrets this also means that secrets can remain un duplicated. 

### Accessing our secrets with ID Tokens

On your `main` branch open the `.gitlab-ci.yml` file and add in a new job:

```yaml
ID_TOKENS_VAULT_V1:
  variables:    
    VAULT_SERVER_URL: http://10.138.0.2:8200
    VAULT_AUTH_PATH: jwt_v2
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://example.gitlab.com
  rules:
    - if: $CI_COMMIT_REF_NAME =~ /^auto-deploy-*/ # If the branch name starts with `auto-deploy-*` use the production role
      variables:
        VAULT_AUTH_ROLE: vault-training-production
        VAULT_PATH: vault-training/production/db
    - if: $CI_COMMIT_REF_NAME # Otherwise use the staging role
      variables:
        VAULT_AUTH_ROLE: vault-training-staging
        VAULT_PATH: vault-training/staging/db
  secrets:
    SECRETS_DB_PASSWORD:
      vault:
        engine:
          name: kv-v1
          path: secret
        field: password
        path: $VAULT_PATH
      file: false
  script:
    # These variables are masked by default so let's write them to a file and upload that as an artifact
    - echo -n $SECRETS_DB_PASSWORD > password
  artifacts:
    paths:
      - password
```

You can leave the previous job in there if you wish, or comment it out, or remove it, up to you! You may wish to leave it so you can toggle `Limit JSON Web Token (JWT) access` off and on to see what happens with both jobs.

Now is a good time to go in to the project settings and toggle `Limit JSON Web Token (JWT) access` to enabled as well so this job works.

This job is named `ID_TOKEN_VAULT_V1` because it uses ID Tokens and is designed for use against version 1 of Vaults keystore. Hopefully this makes a bit more sense in a moment if you are confused!

Similarly to the first CI example you will need to update `variables: VAULT_SERVER_URL:` to the correct details for your Vault server, you should also update `id_tokes: VAULT_ID_TOKEN:` to the URL of your GitLab instance.

Some other things of note are:
  - We define `VAULT_AUTH_PATH` to match the value we used in `-path` earlier. If you changed this when you configured the new authentication path then update it here. This defaults to `jwt` so if you were setting this up from scratch you would not need to specify it
  - We have the same `rules:` section as before to set some variables up depending on the branch we're on. This is even more important now as if you try and access a secret from a branch that you shouldn't the CI will error and stop very early on when secrets are resolved.
    - Because we cannot attempt to access a secret we do not have access to we also define `VAULT_PATH` as the path of the correct secret for that branch. This is the same as the path you used at the start when you wrote secrets with `vault kv put` but without the `secret/` prefix.

You can see a full breakdown of the `secrets:` options in the [.gitlab-ci.yaml reference](https://docs.gitlab.com/ee/ci/yaml/#secrets) along with some other examples.

New customers deploying Vault x1.2.0 or later will have a version 2 keystore and can take advantage of the short syntax detailed there, however a lot will have legacy installations that have been upgraded (or not) and simply upgrading Vault does not convert your key store from v1 to v2.

Lets take a closer look:

```yaml
  secrets:
    SECRETS_DB_PASSWORD:
      vault:
        engine:
          name: kv-v1
          path: secret
        field: password
        path: $VAULT_PATH
      file: false
```

- `SECRETS_DB_PASSWORD` - Variable name that the secret will be stored in
- `vault: engine:` - Tell the CI that we're using the Vault secret engine
  - Both `name:` and `path:` must be set if you specify `engine:`
  - `name:` will default to `kv-v2`
  - `path:` will default to `data`
  - As discussed we are using the v1 engine and our examples use `secret` as a path so we override these defaults
- `field:` - the `field` used when running `vault kv put`
- `path:` - path to our secret without `secret/` as we have set that under `vault: engine: path:`
- `file:` - By default the ID Tokens system fills the variable (`SECRETS_DB_PASSWORD`) with a path to a file which contains the secret, rather than putting the secret in to the variable. A lot of modern tools will let you specify a file rather than the raw password on the command line to avoid leaking secrets in logs etc. There's a choice to be made here but as the old method provided the password in the variable we set `file: false` to replicate that.

We automatically mask these variables which is a great change but it makes it difficult to check the contents is correct as we also mask other strings (such as `(nil)` which you can sometimes get back if something is wrong) so in the last part of the CI job we echo the `SECRETS_DB_PASSWORD` variable in to a file and upload it as an artifact so we can verify everything.

This is not very secure so you wouldn't do it in the real world.

A safer option could be to generate a `md5sum` and compare those but we are just testing here.

Save the `.gitlab-ci.yml` to the `main` branch and let the pipeline run. Both jobs should succeed from GitLabs point of view as the `CI_JOB_JWT` job does not return a failure code but if you examine the log you will it has actually failed.

Check out the artifacts for `ID_TOKENS_VAULT_V1` and you should see your password in the `password` file.

Fork `main` in to a new branch starting with `auto-deploy-` and check the results of that pipeline.

Congratulations, you have converted this project over to the new ID Tokens method without breaking all of your other projects!

## Some additional exercises

### Run Vault with version 2 of its keystore

Remember that because we have Vault in developer mode everything is in memory so as soon as you stop the process you will have to start again with everything you have done so far, however if you want to repeat the exercise with a version 2 key store you can do so by stopping `vault` then running it again without `-dev-kv-v1`, e.g.:


```bash
vault server -dev -dev-listen-address "0.0.0.0:8200"
```

See if you can setup a CI job to read a secret using the [short syntax](https://docs.gitlab.com/ee/ci/yaml/#secretsvault) method.

### Adding more than one field to a secret path

It is possible to add more than one field to a secret path.

This would let you store say the username and password, or even a SSL certificate/key as well.

Try and extend the examples to do this.

### Trying to access a secret we don't have permission for

In "Accessing our secrets with ID Tokens" I said you cannot attempt to access a secret you don't have permission for.

In the `CI_JOB_JWT` job we accessed both with and while we got an error in the script we could continue.

Try modifying the `ID_TOKENS_VAULT_V1` job to access the wrong secret and observe what happens.
