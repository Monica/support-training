---
module-name: "Container Registry"
area: "Product Knowledge"
gitlab-group: "Package:Package"
maintainers:
  - TBD
---

## Overview

**Goal**: Set a clear path for GitLab Container Registry Expert training.

**Objectives:**

- Learn about GitLab Container Registry and how to utilize it.
- Learn some of the more complex tasks of using the Container Registry.

---

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: `GitLab Container Registry - <your name>`
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Container Registry questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you've started learning this knowledge area:
   
   ```
   knowledge_areas:
   - name: Container Registry
     level: 1
   ```

## Stage 1: Become familiar with what GitLab Container Registry is

- [ ] **Done with Stage 1**

1. [ ] Learn about GitLab Container Registry
    1. [ ] Read [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
    User documentation
    1. [ ] Read [ GitLab Container Registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html)
    Administration documentation.
    1. [ ] Understand the difference between [GitLab's Registry fork](https://gitlab.com/gitlab-org/container-registry) and the [upstream project](https://github.com/distribution/distribution)

## Stage 2: Technical setup and exploring

- [ ] **Done with Stage 2**

1. [ ] Setup a self-managed instance to use GitLab Container Registry. Ensure you try with:
    1. [ ] [Changing the registry's internal port](https://docs.gitlab.com/ee/administration/packages/container_registry.html#change-the-registrys-internal-port)
    1. [ ] Using both self-signed and Let's Encrypt SSLs
1. [ ] Create a project
    1. [ ] [Build and push an image from your local machine](https://docs.gitlab.com/ee/user/packages/container_registry/#build-and-push-images-by-using-docker-commands)
    1. [ ] [Build and push an image using GitLab CI/CD](https://docs.gitlab.com/ee/user/packages/container_registry/#build-and-push-by-using-gitlab-cicd)
    1. [ ] Set up an [External Container Registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html#use-an-external-container-registry-with-gitlab-as-an-auth-endpoint)
    1. [ ] Explore the [Container Registry API](https://docs.gitlab.com/ee/api/container_registry.html) and try out [bulk deleting tags](https://docs.gitlab.com/ee/api/container_registry.html#delete-registry-repository-tags-in-bulk)
    1. [ ] Learn about the [`gitlab-ctl registry-garbage-collect` command](https://docs.gitlab.com/ee/administration/packages/container_registry.html#recycling-unused-tags)
    1. [ ] Understand how Docker registry authentication works by reading the [registry](https://docs.gitlab.com/ee/administration/logs.html#registry-logs) and GitLab logs. Also refer to https://docs.docker.com/registry/spec/auth/token/

## Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer up to 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers. If you're having trouble findings tickets on this topic consider asking for an experts review on the tickets completed.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Quiz

- [ ] **Done with Stage 4**

   1. [ ] Schedule a call with a current Container Registry Expert. During this call, you
          will guide them through the following:
      1. [ ] Using a self-managed instance with a self-signed certificate([steps for becoming your own CA](https://stackoverflow.com/a/60516812)):
        1. [ ] Build a simple docker file and push it to the registry:

            On a local machine, create a file called `Dockerfile` and add the following content:

            ```docker
            FROM ubuntu:latest

            RUN apt update

            EXPOSE 8080
            ```

            Then authenticate against the registry and build, then push the image:
            [Reference the docs](https://docs.gitlab.com/ee/user/packages/container_registry/#authenticating-to-the-gitlab-container-registry)

            ```bash
            docker login registry.example.com

            docker build -t registry.example.com/group/project/image .

            docker push registry.example.com/group/project/image
            ```

            Be sure to get the [Docker client to trust the self-signed certificate](https://docs.docker.com/registry/insecure/#use-self-signed-certificates)

   1. [ ] Once you have completed this, have the expert comment below
          acknowledging your success.

## Penultimate Stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

## Final Stage: Completion

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: Container Registry
     level: 2
   ```

/label ~module
/label ~"Module::Container Registry"
/assign me
