---
module-name: "Zendesk Basics"
area: "Customer Service"
maintainers:
  - atanayno
  - support operations
  - support managers
---

### Introduction

Zendesk is our Support Center and the main communication line with our customers. This module will bootstrap you to work with Zendesk efficiently as a GitLab Support Engineer.

**Goals of this checklist**

At the end of the checklist you will be able to:
- Utilize Zendesk to perform ticket management tasks
- Access the CustomersDot (customer portal) to look up customer & account information
- Use helper tools for ZenDesk

**General Timeline and Expectations**

- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take you **1 day to complete**.

### Stage 1: Prerequisites

- [ ] **Done with Stage 1**
<details>
<summary>Tasks: click to expand/contract</summary>

   Review the Zendesk resources listed below:

  1. [ ] Complete Zendesk Agent training
     1. [ ] Sign up at [Zendesk University](https://training.zendesk.com/auth/login?next=%2F).
     You'll receive an email with information on accessing the Zendesk courses
     1. [ ] Complete the [Zendesk Overview: Support](https://training.zendesk.com/introduction-to-foundational-support) course (approx. 10 min)
  1. [ ] Review additional Zendesk resources and our Zendesk workflow
     1. [ ] [Zendesk: UI Overview](https://support.zendesk.com/hc/en-us/articles/203661806-Introduction-to-the-Zendesk-agent-interface)
     1. [ ] [Zendesk: Updating Tickets](https://support.zendesk.com/hc/en-us/articles/212530318-Updating-and-solving-tickets)
     1. [ ] [Zendesk: Working w/ Tickets](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets)
        *(Read [Avoiding Agent Collisions](https://support.zendesk.com/hc/en-us/articles/203690856-Working-with-tickets#topic_ryy_42g_vt) carefully)*
     1. [ ] [Zendesk: Using Macros](https://support.zendesk.com/hc/en-us/articles/203690796-Using-macros-to-update-tickets)
     1. [ ] [Zendesk: Triggers and how they work](https://support.zendesk.com/hc/en-us/articles/203662246)
     1. [ ] [Zendesk: Formatting text with Markdown](https://support.zendesk.com/hc/en-us/articles/203691016-Formatting-text-with-Markdown)
     1. [ ] GitLab: Useful tools for [copying internal notes/drafts to public replies](https://about.gitlab.com/handbook/markdown-guide/#markdown-editors)
            and for [spell-checking](https://about.gitlab.com/handbook/tools-and-tips/other-apps/#languagetool)
     1. [ ] [Zendesk: Support search reference](https://support.zendesk.com/hc/en-us/articles/203663226-Zendesk-Support-search-reference)
     1. [ ] [GitLab: working with macros in Zendesk](https://handbook.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_macros/)
     1. [ ] [GitLab: translation using Unbabel for Zendesk](https://handbook.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_apps/#unbabel-for-zendesk-support)
     1. [ ] [GitLab: triggers in Zendesk](https://handbook.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_triggers/).
     1. [ ] [GitLab: Marking tickets as spam in Zendesk](https://handbook.gitlab.com/handbook/support/workflows/marking_tickets_as_spam/); more details on the [alternate way](https://support.zendesk.com/hc/en-us/articles/203691106-Marking-a-ticket-as-spam-and-suspending-the-requester) to mark tickets as spam.
  1. [ ] Check how your personal Zendesk signature looks like.
     1. [ ] In Zendesk, click your user icon in the upper-right corner, select **View Profile** and check the **Signature** field.
  1. [ ] (optional) Determine if you want personalized salutation such as `Thanks` or `Best regards` in your Zendesk signature
     * You can add these by editing the [support-team.yml](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) file, but it's better to end each message with words that match the situation of that ticket. 

</details>

### Stage 2: Studying ticket lifecycle

- [ ] **Done with Stage 2**
<details>
<summary>Tasks: click to expand/contract</summary>

  This section describes the common lifecycle of a ticket and explains how to use some Zendesk features while working with tickets.
  Each theoretical part is accompanied by some practical steps. Be sure to go through all the practical steps to make sure that you understand Zendesk behavior
  correctly in each case.

  We have created a test organization, [`Zendesk Basics Module`](https://gitlab.zendesk.com/agent/organizations/367958101679/users), for this Module. Please use it for all the tasks below.


  1. [ ] **Ticket statuses:** read the article about [ticket statuses](https://handbook.gitlab.com/handbook/support/support-ops/documentation/ticket_statuses/)
  to understand how we use them in GitLab Support.

  1. [ ] **Creating a new ticket:**

      ##### 1. How customers can create tickets

      GitLab customers can submit tickets by navigating to our [Support Portal](https://support.gitlab.com/hc/en-us/requests/new) and completing the support form.

      ##### 2. How support agents can create tickets

      Sometimes you may need to create a new ticket for a customer. Common use cases include:
       - Creating a follow-up ticket for an emergency
       - A ticket is too long and it contains a mix of various issues, so you want to move one of those issues to a new ticket
      <br>

     To create a ticket as a support agent:
       - Open our [Support Portal](https://support.gitlab.com/hc/en-us/requests/new) in a new browser (or via incognito mode)
       - Select the correct form to use
       - Fill out the form information relevant to the customer you are submitting the ticket for
         - Make sure to use the customer's email, not your own
       - Submit the ticket
       <br>

      **Notes:**
       - It is highly recommended to create a ticket as a customer, not as an agent. In this case, it will get proper SLA immediately.
        - After a support agent creates and submits a ticket as `New` or `Open`, the ticket will have no SLA. SLA is assigned only when a customer updates a ticket.
        If you decided to create a ticket on your own, it is recommended to send a reply to the customer and submit a ticket as `Pending`, `Solved` or `On-hold`, depending on the situation.
        - When a ticket is submitted by an unknown user, a user account will be automatically created by Zendesk.
        <br>

      **Practice:**
     - [ ] Create a ticket pretending you are a customer by completing the form from the [Support Portal](https://support.gitlab.com/hc/en-us/requests/new). Use the following values for each field. Once you fill out the form, submit it and look for an automated reply in your personal email.
          |Field                                                                |Value                                                                   |
          |---------------------------------------------------------------------|------------------------------------------------------------------------|
          |**Choose the reason why you are reaching out to us today**           |`Support for GitLab.com (SaaS)`                                         |
          |**Your email address**                                               |Your personal email                                                     |
          |**Subject**                                                          |`***IGNORE*** Test ticket only for ZD Basics Onboarding ***IGNORE***`   |
          |**Description**                                                      |Add a description that reiterates that this is a test and can be ignored|
          |**Tell us about your GitLab subscription**                           |`Free user`                                                             |
          |**Problem type**                                                     |`Project in broken state`                                               |
          |**GitLab.com Username**                                              |Your GitLab.com username                                                |
          |**GitLab.com Project Path**                                          |`test/test`                                                             |
          |**What is the ticket severity?**                                     |`Severity 4`                                                            |
          |**Priority**                                                         |`Low`                                                                   |
          |**Preferred region for support**                                     |Select the appropriate region for you                                   |
      - [ ] Auto-responder will ask you to provide a support entitlement. Reply to the auto-responder via e-mail with any message.
      - [ ] Log in as an agent and find the new ticket by searching for the ticket name
      - [ ] Take ownership of the ticket by using the "take it" link in the upper left area
      - [ ] Log in to Zendesk as the end-user that you used to create a test ticket and explore how it looks like in this mode. To get a password, 
        you will need to click **Sign in > Get a password** in the [Support Portal](https://support.gitlab.com/hc/en-us).

      <br>
  1. [ ] **Modifying CC list in tickets**. To add yourself, a customer or another GitLab
     team member to the CC list in the ticket:
     - Open the ticket in the agent interface.
     - Copy the email address you need to add and paste it to the `CCs` field.
     - Click `Submit` to save your change.
       - Note that this will *save* any text that you have entered in the "Public reply" or
       "Internal note" field. Be aware that in case of having entered text in the "Public reply" field, this
       content will be sent as a response to the customer upon doing so. This happens whether the ticket is
       submitted in the same state or in a new state.
     - Zendesk will send only public replies to non-GitLab users who are included in the CC list of a ticket,
       whereas it will send all replies, public or internal, to the Support Agents in the CC list.
     <br>

     **Note:** At the moment the ability to add addresses to the CC list via the support portal
     is disabled due to [this security issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1278),
     which is why we need to perform this action for customers.

     If you need to remove someone from the CC list:
        - Open the ticket in the agent interface.
        - Click the cross near the specified email address to remove it and click `Submit` to save the change.
      <br>

      **Practice:**
      - [ ] Add your work email address to the CC list of the previously created ticket.
      <br>
  1. [ ] **Changing a ticket's form and sending replies:**

     In most cases the customer selects the correct ticket form when they submit a ticket.
     If the specified form looks wrong, likely the customer made a mistake. Given this possibility, please always review the
     contents of a new ticket and do the following:

     - If the current form used and the form that is needed is SaaS, SaaS Account, or Self-Managed, you can change the form on the current ticket.
     - If the above is not true, use the macro `General::Forms::Incorrect form used`

      <br>

      **Practice:**
      - [ ] As a support agent, send a reply to the ticket and set the ticket to **Pending**.
      - [ ] You will get a message in your personal email. Now reply back from your personal email
        and check Zendesk; you will see that the ticket is back in the views,
      it has **Open** status and SLA is shown in green.
    
      **Note**: there is a limitation on Zendesk side: [markdown only works for support agents, not for customers](https://support.zendesk.com/hc/en-us/community/posts/360038476034-Markdown-for-customers). 
      <br>
  
  1. [ ] **Using macros in tickets**: a macro is a message template that you can use as a starting point for a public
     response in a common situation, such as:
        - You need to change the form of a ticket from Self-Managed to GitLab.com or vice versa.
        - You need to provide a CE user with information about free support resources.
        - You need to explain to a GitLab.com user what they can do to reset 2FA.
        - After the call with a customer, you would like to summarize everything that was done during the call.

      Please edit the message contents after applying the macro so that the message
      is delivered in your own voice and is appropriate for the ticket.

      To add a macro to your reply:
       - Open a ticket.
       - Click the `Apply Macro` button.
       - Search for the macro if you know its name or find it in the list, and then
         click it. The macro will be added to the comment field of your ticket.
      <br>

      **Practice:**
      - [ ] Let's pretend that you scheduled a call with customer. In order to meet the SLA,
        reply to the ticket and set it to **On-hold**.
        Note that this action will reset the SLA clock and assign the ticket to you.
        If you leave it in the **On-hold** status for 4 days, it will be reopened but there will be no SLA.
        If you want to put a ticket to **On-hold** once again, you will need to send another public reply to do it.
      - [ ] Suppose that you held a call with a customer and fixed the problem they had reported.
        Add the **Post-customer call** macro to the ticket, fill in the template provided
        by the macro and set the ticket's status to **Solved** when sending the update.
        Check that you got email messages in your personal email inbox regarding all these actions.

        **Note:** if you want to put a ticket to **On-hold** status for a longer period of time, utilize our GitLabReminders App,
        see [GitLab Reminders App](https://handbook.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_apps/#gitlab-reminders-app) & [GitLab Reminders App
 project](https://gitlab.com/gitlab-com/support/support-ops/zendesk-apps/gitlab-reminders-app) for details.
      <br>

  1. [ ] **Using Unbabel to translate your replies:**
  
     If a customer's reply was sent in [one of non-English languages supported by us](https://handbook.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_apps/#languages-we-support),
     we should reply using Unbabel - automatic translation tool built as Zendesk app. Please do not use it with test tickets though, as they are paid for us.
     Instead, it is recommended to read [Unbabel for Zendesk Support](https://handbook.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_apps/#unbabel-for-zendesk-support) and review existing tickets where Unbabel was used to learn more about the syntax specifics. You can also find the [recorded training session and slides](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1805) provided by Unbabel when it was first introduced to GitLab.
      <br>
  
      **Practice:**
      - [ ] Find some tickets translated using Unbabel. You may do it by searching for tickets with `unbabeled` tag
      - [ ] Briefly review 2-3 tickets and check how Unbabel syntax was used in practice.
      
      **Note:** if a ticket has any status other than Closed, `#unbabel` keyword will be automatically inserted to the internal comment field.
      Make it a habit to remove those comments as they can be confusing to others: if you leave a comment there, Zendesk will show that you are editing a ticket. 
      <br>

 1. [ ] **Changing Priority and using a macro** This exercise combines a couple
     of the previous exercises. Start by reading about [ticket priority](https://handbook.gitlab.com/handbook/support/workflows/setting_ticket_priority/).

      **Practice:**

      - [ ] Change the priority on one of your tickets and use the appropriate macro as part of your response.
      <br>

 1. [ ] **Merging tickets:** Sometimes customers create duplicate tickets. In order to avoid
  confusion it is a good idea to merge them into one.
  Let's imagine we have tickets `0001` and `0002`, and we want to merge `0002` into `0001`. To do it:
      - Open the ticket `0002`.
      - Click the arrow to the far right of the ticket subject, and select `Merge into another ticket`.
      - The `Ticket merge` dialogue will appear. Enter the ticket ID `0001` into the field `Enter ticket ID to merge into` and click `Merge`.
      - In the next window it is important to pay attention to the `Requester can see this comment` checkboxes.
      It is recommended to ensure the checkbox is unchecked for both tickets.
      - **Important:** note that it is [not possible to unmerge tickets](https://support.zendesk.com/hc/en-us/articles/115004245847-Can-I-un-merge-tickets-). Be careful and thoroughly check everything before merging tickets.
      <br>

      **Practice:**
      - [ ] Create two tickets using the [Support Portal](https://support.gitlab.com/hc/en-us/requests/new). After that, as an agent, merge the second ticket into the first one.
      <br>

1. [ ] **Viewing original email message:**

     Sometimes you will see that there is an envelope icon in the upper right part of the customer’s comment.
     It can contain some useful information such as:
        - original formatting of the e-mail message
        - the list of original recipients that the customer attempted to CC
        - replies to questions that the customer has placed inline in the quoted text

     To view the original email message, follow the steps from [How can I view the original recipients of a ticket I received in Zendesk Support?](https://support.zendesk.com/hc/en-us/articles/231736808-How-can-I-view-the-original-recipients-of-a-ticket-I-received-in-Zendesk-Support-)
      <br>

      **Practice:**
      - [ ] Send a reply as an end user to one of your test tickets. Use your e-mail agent to do it, 
      add some random e-mail to CC and apply any formatting you like.
      - [ ] Open this ticket as an agent in Zendesk. You will see that the formatting and CC list is not reflected there. 
      Open the original email message to see them.
      <br>

  1. [ ] **Cleanup:** after you are done with all the tests, please close all your tickets:
      - You may mark them as **Solved**. Such tickets will move to **Closed** automatically after 7 days according to
      [Understanding Ticket Status](https://handbook.gitlab.com/handbook/support/workflows/working-on-tickets/#understanding-ticket-status).
      - You may mark them as **Pending** but note that if you do so for a ticket that has SLA, you should send a public reply when moving it to **Pending**.
      After 20 days in the **Pending** status, the ticket will be marked as **Solved**.
      - Make sure that there are no test tickets left in **On-hold** status as they will move to **Open** in 4 days.
      - It is considered a good practice to delete your user and all the test tickets you created. 
        To delete a test ticket, click the arrow to the far right of the ticket subject > **Delete > Ok**.
        To delete a test user, click the user, find the same arrow icon and click Delete > Ok.

</details>

### Stage 3: Learning about Zendesk from the Support Operations point of view

- [ ] **Done with Stage 3**

Our Support Operations team has prepared some materials about how we use Zendesk that you may find useful. 

(NOTE: The materials were compiled February/March of 2021 and some information is outdated, please check the [handbook](https://handbook.gitlab.com/handbook/support/support-ops/documentation/#zendesk-global) for the most up-to-date information about Zendesk)

Watch the below-listed videos and review presentations associated with them: 

- [ ] Watch videos from the playlist [State of Zendesk Global](https://www.youtube.com/playlist?list=PL05JrBw4t0KqsKi4iaIOWDvr6J4w__GAb): around 35 min totally
- [ ] Review the presentations associated with the videos if you prefer text over videos:
  - [ ] [Zendesk as a whole](https://docs.google.com/presentation/d/1UlWmpwoj-af9adRmHgyHOiHanWcBiw2KXWJUaVU6W04/edit#slide=id.gb8b29294db_0_189)
  - [ ] [Zendesk as an agent](https://docs.google.com/presentation/d/1osweAZEVi9BXcQr2cvoezVvIfKyYNh6N8Qz-VFjeuAQ/edit#slide=id.gb44079cb20_0_0)
  - [ ] [Zendesk apps](https://docs.google.com/presentation/d/1YeGb0OOg_1JyTZa8_gA-LJAU5JI_TwMmvttfxtyy8Js/edit#slide=id.gb8e285ac8b_0_201),
      similar info is also available at [Zendesk Apps handbook page](https://handbook.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_apps/)
  - [ ] [Zendesk as an end user](https://docs.google.com/presentation/d/1DKU-ivXPjuVkoPyUqciqR04OXjYa361qCF8v4CxbbVQ/edit#slide=id.gb40153e8cd_0_0)
  - [ ] [How GitLab is using Zendesk Part 1](https://docs.google.com/presentation/d/1ckcyH5f9MCmjXpi-7aDdR63_TdGRjprBZcwsa2h3O34/edit)
  - [ ] [How GitLab is using Zendesk Part 2](https://docs.google.com/presentation/d/1398SCAjb_cAOjM8R_d_lUfMst4JpNXDm8_L4pseddDM/edit#slide=id.g796c3f3f65_0_367)

### Stage 4: Zendesk Instances

- [ ] **Done with Stage 4**

<details>
<summary>Tasks: click to expand/contract</summary>

  - [ ] Read about [the different Zendesk instances](https://handbook.gitlab.com/handbook/support/support-ops/documentation/zendesk_instances/).
  - [ ] If you are a U.S. Citizen, you will be working in the U.S. Federal instance in addition to the standard instance. 
All other team members should be aware that there is a U.S. Federal instance and who it's for.

</details>

### Stage 5: Looking up Customer & Account Information
- [ ] **Done with Stage 5**
<details>
<summary>Tasks: click to expand/contract</summary>


We use several tools to look up customer & account information.


**Customer Portal and LicenseDot App**

1. [ ] Familiarize yourself with the workflow for [looking up customer information](https://handbook.gitlab.com/handbook/support/workflows/looking_up_customer_account_details/).
1. [ ] Read about and be aware of the [CustomersDot](https://handbook.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html#within-customersgitlabcom), and the [LicenseDot](https://handbook.gitlab.com/handbook/support/workflows/looking_up_customer_account_details/#within-licensegitlabcom) apps. 

If you will primarily focus on License & Renewals, you should be given access to the above tools, along with training on the [License & Renewals workflows](https://handbook.gitlab.com/handbook/support/license-and-renewals/workflows/).

</details>

#### Congratulations! You made it, and now have a basic understanding of using Zendesk for ticket management!

You are now ready to continue on your onboarding path to tackle the next module in line based on your role. Reach out to your manager or onboarding buddy if it is not assigned to you yet; you can check the [onboarding page](https://handbook.gitlab.com/handbook/support/training/) or your `New Support Team Member Start Here` issue to see the list of all modules under the Onboarding pathway.

Please also submit MRs for any improvements to this training that you can think of. The file is located in an issue template in the [support-training repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~onboarding
/label ~"Module::Zendesk Basics"
