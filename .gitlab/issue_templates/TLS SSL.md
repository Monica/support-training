---
module-name: "TLS SSL"
area: "Core Technologies"
maintainers:
  - faleksic
---

## Overview

**Goal**: Set a clear path for GitLab TLS / SSL expert training

**Objectives**: At the end of this module, you should be able to:

- Understand the basics of how SSL works.
- Understand how GitLab interacts with SSL.
- Feel comfortable troubleshooting GitLab SSL issues.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: GitLab TLS - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical GitLab TLS questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

### Stage 1: Become familiar with how SSL works

- [ ] **Done with Stage 1**

1. [ ] Read SSL Documentation
   1. [ ] Read [What is SSL?](https://www.ssl.com/faqs/faq-what-is-ssl/)
   1. [ ] Read [What is an SSL certificate?](https://www.digicert.com/ssl/)
   1. [ ] Read [What is a SAN certificate?](https://www.ssl.com/faqs/what-is-a-san-certificate/)
   1. [ ] Read [What is a certificate authority?](https://www.ssl.com/faqs/what-is-a-certificate-authority/)
   1. [ ] Read [What is the difference between a self-signed certificate and a trusted CA signed certificate?](https://cheapsslsecurity.com/blog/self-signed-ssl-versus-trusted-ca-signed-ssl-certificate/)
1. [ ] (Optional) Practice generating SSL certificates
   1. [ ] [Generate a private key](https://www.feistyduck.com/library/openssl-cookbook/online/openssl-command-line/key-generation.html)
   1. [ ] [Create a Certificate Signing Requests](https://www.feistyduck.com/library/openssl-cookbook/online/openssl-command-line/creating-certificate-signing-requests.html)
   1. [ ] [Selfsign Certificates](https://www.feistyduck.com/library/openssl-cookbook/online/openssl-command-line/signing-your-own-certificates.html)
1. [ ] Read GitLab Documentation
   1. [ ] Read [NGINX settings](https://docs.gitlab.com/omnibus/settings/nginx.html)
   1. [ ] Read [SSL Configuration](https://docs.gitlab.com/omnibus/settings/ssl.html)
   1. [ ] Read [Runner SSL documentation](https://docs.gitlab.com/runner/configuration/tls-self-signed.html)

## Stage 2: Technical setup

- [ ] **Done with Stage 2**

1. [ ] Familiarize yourself with [Common SSL Errors](https://docs.gitlab.com/omnibus/settings/ssl/ssl_troubleshooting.html#common-ssl-errors) documentation page.
1. [ ] Configure SSL for GitLab using the [Let's Encrypt integration](https://docs.gitlab.com/omnibus/settings/ssl.html#lets-encrypt-integration).
1. [ ] Configure SSL for [GitLab using a self-signed certificate](https://docs.gitlab.com/runner/configuration/tls-self-signed.html).
1. [ ] Configure GitLab to [trust a self-signed certificate](https://docs.gitlab.com/omnibus/settings/ssl.html#install-custom-public-certificates).
1. [ ] Configure GitLab to [trust a certificate chain](https://docs.gitlab.com/omnibus/settings/ssl.html#install-custom-public-certificates).
1. [ ] Configure a [Runner to trust a self-signed certificate](https://docs.gitlab.com/runner/configuration/tls-self-signed.html#trusting-the-certificate-for-user-scripts).
1. [ ] Configure a [Runner to trust a certificate chain](https://docs.gitlab.com/runner/configuration/tls-self-signed.html#supported-options-for-self-signed-certificates-targeting-the-gitlab-server).

## Stage 3: Working with GitLab and SSL

- [ ] **Done with Stage 3**

Remember to contribute to any documentation that needs updating.

1. [ ] Look for 10 old SSL-related tickets and read through them to understand what the issues were and how they were addressed. Paste the links here.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

1. [ ] Answer 5 SSL-related tickets and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Pair on Customer Calls (Optional)

- [ ] **Done with Stage 4**

1. [ ] Pair on up to two Customer Calls, where a customer is having trouble with SSL.
   1. [ ] call with ___
   1. [ ] call with ___

## Stage 5: Quiz

- [ ] **Done with Stage 5**

Schedule a call with a [TLS SSL Expert](https://gitlab-com.gitlab.io/support/team/skills-by-subject.html) (search for `SSL`). During this call, you will guide them through the following:

  1. [ ] Clone the [support-training](https://gitlab.com/gitlab-com/support/support-training) project as it contains files needed for next steps in the `content/TLS SSL` folder.
     1. [ ] Print the Subject Alternative Name(s) that the `example.crt` SSL certificate covers.
     1. [ ] Given the files `example-1.key` and `example-2.key`, determine which one belongs to the `example.crt` file ([relevant troubleshooting page](https://docs.gitlab.com/ee/administration/troubleshooting/ssl.html#x509-key-values-mismatch-error))
  1. [ ] Print the validity time period for the `gitlab.com` SSL certificate
  1. [ ] Print the certificate chain for the `gitlab.com` SSL certificate
  1. [ ] Given the error `fatal: unable to access 'https://.git. : SSL certificate problem: unable to get local issuer certificate` - name the cause and any possible solutions (hint: [SSL Configuration](https://docs.gitlab.com/omnibus/settings/ssl.html) documentation page).
  1. [ ] Once you have completed this, have the expert comment below acknowledging your success.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as GitLab SSL Expert on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module
/label ~"Module::TLS SSL"
/assign me