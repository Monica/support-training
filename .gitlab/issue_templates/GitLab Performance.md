---
module-name: "GitLab Performance"
area: "Troubleshooting & Diagnostics"
maintainers:
  - wchandler
---

## Overview

**Goal**: <general goal of module>

**Objectives**: At the end of this module, you should be able to:

- <insert a few concrete actions that learner should be able to do at the end of this module>

Tackle stage 1 first and the last stage last, but the others can be completed in
any order you prefer.

## Stage 0: Commit to learning about GitLab Performance

- [ ] **Done with Stage 0**

1. [ ] Create an issue using this template by making the Issue Title: GitLab Performance - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical GitLab Performance questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: GitLab's Architecture & Internals

- [ ] **Done with Stage 1**

This stage is aimed for you to have a good grasp of GitLab internals.

1. [ ] Review GitLab's Architecture, Components, Request Type & System Layout
    1. [ ] GitLab's [Architecture Overview](https://docs.gitlab.com/ee/development/architecture.html)
    1. [ ] GitLab.com's [Production Architecture overview](https://about.gitlab.com/handbook/engineering/infrastructure/production-architecture/)
    1. [ ] GitLab.com's [cloud native high level architecture](https://docs.google.com/drawings/d/1mAC0qEEVHzHqy4kbcnDka0TVikzttDSJBrq1hQzQ5-Q/edit)

## Stage 2: GitLab's HA Installation methods

- [ ] **Done with Stage 2**

This stage is aimed at you having a high level understanding of the different High-Availability Installation methods.

1. [ ] [Geo](https://docs.gitlab.com/ee/administration/geo/replication/index.html)
1. [ ] [Scaling and High Availability](https://docs.gitlab.com/ee/administration/reference_architectures/#deciding-which-architecture-to-use/)
1. [ ] [GitLab HA on AWS](https://docs.gitlab.com/ee/install/aws/index.html)
1. [ ] [Cloud Native charts](https://docs.gitlab.com/charts/)

## Stage 3: GitLab/Linux Performance Analysis & Troubleshooting

- [ ] **Done with Stage 3**

This stage lists resources to tools/performance topics that are usually experienced by GitLab's customers

1. [ ] Familiarity GitLab Support Toolbox
    1. [ ] [GitLabSOS](https://gitlab.com/gitlab1.com/support/toolbox/gitlabsos)
    1. [ ] [GitLabSOS Analyzer](https://gitlab.com/gitlab-com/support/toolbox/sos-analyzer)
    1. [ ] [strace-parser](https://gitlab.com/gitlab-com/support/toolbox/strace-parser)
1. [ ] GitLab Tools
    1. [ ] [GitLab Performance Bar](https://docs.gitlab.com/ee/administration/monitoring/performance/performance_bar.html)
1. [ ] Git Performance:
    1. [ ] [Troubleshooting Git](https://docs.gitlab.com/ee/topics/git/troubleshooting_git.html)
    1. [ ] [git-fsck](https://git-scm.com/docs/git-fsck)
    1. [ ] [git-sc](https://git-scm.com/docs/git-gc)
    1. [ ] [git prune](https://www.atlassian.com/git/tutorials/git-prune)
1. [ ] PostgreSQL Performance
    1. [ ] GitLab's [Database settings](https://docs.gitlab.com/omnibus/settings/database.html)
    1. [ ] [Configure GitLab using an external PostgreSQL service](https://docs.gitlab.com/ee/administration/external_database.html)
    1. [ ] [Configuring PostgreSQL for Scaling/High Availability](https://docs.gitlab.com/ee/administration/high_availability/database.html)
    1. [ ] [Understanding Postgres Performance](http://www.craigkerstiens.com/2012/10/01/understanding-postgres-performance/)
1. [ ] AWS Rate Limits
    1. [ ] [EFS Rate Limits](https://docs.aws.amazon.com/efs/latest/ug//limits.html)
    1. [ ] [Other AWS Rate Limits](https://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html)
1. [ ] Linux System Performance Tracing/Analysis
    1. [ ] [Linux Performance Analysis in 60,000 Milliseconds](https://medium.com/netflix-techblog/linux-performance-analysis-in-60-000-milliseconds-accc10403c55)
    1. [ ] [BPF(Berkeley Packet Filters) Compiler Collection](https://github.com/iovisor/bcc)
    1. [ ] [Linux Performance and Tuning Guidelines](https://lenovopress.com/redp4285.pdf) [PDF]
    1. [ ] [Linux kernel profiling with perf](https://perf.wiki.kernel.org/index.php/Tutorial)
    1. [ ] [perf Examples](http://www.brendangregg.com/perf.html)
    1. [ ] Brendan Gregg's [Linux Performance](http://www.brendangregg.com/linuxperf.html)

## Stage 4: Tickets

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 5: Pair on customer calls

1. [ ] Pair on or perform five diagnostic calls, where a customer is having trouble with GitLab Performance.
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___
   1. [ ] call with ___

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as GitLab Performance Expert on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module
/label ~"Module::GitLab Performance"
/assign me