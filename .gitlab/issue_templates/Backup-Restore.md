---
module-name: "Backup-Restore"
area: "Product Knowledge"
gitlab-group: "Enablement:Geo"
maintainer:
  - Manu, @manuelgrabowski
---

**Goal of this checklist:** Set a clear path for Backup-Restore training


## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: Backup-Restore - "your name"
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you've started learning this knowledge area:
   
   ```
   knowledge_areas:
   - name: Backup-Restore
     level: 1
   ```


## Stage 1: Become familiar with the basic Backup-Restore process

- [ ] **Done with Stage 1**

1. [ ] Read about the basics of taking a backup
   1. [ ] [Requirements](https://docs.gitlab.com/ee/raketasks/backup_restore.html#requirements)
      1. [ ] [gitaly-backup](https://docs.gitlab.com/ee/raketasks/backup_restore.html#gitaly-backup-for-repository-backup-and-restore)
      1. [ ] [Backup Timestamp](https://docs.gitlab.com/ee/raketasks/backup_restore.html#backup-timestamp)
   1. [ ] [GitLab Backup](https://docs.gitlab.com/ee/raketasks/backup_gitlab.html)
      1. [ ] *Important* [Storing Configuration Files](https://docs.gitlab.com/ee/raketasks/backup_gitlab.html#storing-configuration-files)
      1. [ ] [Excluding Directories](https://docs.gitlab.com/ee/raketasks/backup_gitlab.html#excluding-specific-directories-from-the-backup)
      1. [ ] [Incremental Backups](https://docs.gitlab.com/ee/raketasks/backup_gitlab.html#incremental-repository-backups)

      - Feel free to read up on all of the other documentation, we are focusing on the Omnibus backup at this step

1. [ ] Read about the basics of restoring a backup
   1. [ ] [Restore prerequisites](https://docs.gitlab.com/ee/raketasks/restore_gitlab.html#restore-prerequisites)
      1. [ ] [GitLab Restore](https://docs.gitlab.com/ee/raketasks/restore_gitlab.html)
      1. [ ] [Omnibus Restore](https://docs.gitlab.com/ee/raketasks/restore_gitlab.html#restore-for-omnibus-gitlab-installations)
      1. [ ] [Restore Options](https://docs.gitlab.com/ee/raketasks/restore_gitlab.html#restore-options)

      - Again, feel free to read up on additional documentation, but the Omnibus restore will be the basic focus for this module

## Stage 2: Hands On

- [ ] **Done with Stage 2**

1. **Backup**
   1. [ ] Setup a personal GitLab instance (this can be one you use personally now or a test instance you spin up) *Omnibus preferred*
   1. [ ] Recommended to have some data pushed to your instance but not required *optional*
   1. [ ] Successfully create a full backup with the command `sudo gitlab-backup create`
   1. [ ] Safely copy and save configuration files (preferably off-site of your gitlab instance):
      1. [ ] /etc/gitlab/gitlab-secrets.json
      1. [ ] /etc/gitlab/gitlab.rb
   1. [ ] Verify that the backup is in the correct location - `default location → /var/opt/gitlab/backups` (Unless configured in your gitlab.rb file to another location)


1. [ ] *Bonus points - Not Required* Setup a backup using a [crontab](https://docs.gitlab.com/ee/raketasks/backup_gitlab.html#configuring-cron-to-make-daily-backups)
  


1. **Restore**

*Side note* - Its more fulfilling if you wipe your current instance before restoring.  This gives you a clear way to verify things were restored in the correct location and that your configuration files are also correctly restored.

```
This procedure assumes that:

 - You have installed the exact same version and type (CE/EE) of GitLab Omnibus with which the backup was created
 - You have run `sudo gitlab-ctl reconfigure` at least once
 - GitLab is running. If not, start it using `sudo gitlab-ctl start`
```

   1. [ ] First ensure your backup tar file is in the backup directory described in the `gitlab.rb` → configuration `gitlab_rails['backup_path']` and owned by the git user.
   1. [ ] Copy the timestamp from your backup file.  Example → `11493107454_2018_04_25_10.6.4-ce_gitlab_backup.tar` copy `11493107454_2018_04_25_10.6.4-ce`
   1. [ ] Proceed to run the restore command → `sudo gitlab-backup restore BACKUP=11493107454_2018_04_25_10.6.4-ce` (using the timestamp you copied earlier)
   1. [ ] Replace the configuration files with the ones you have saved in the backup process
      1. [ ] /etc/gitlab/gitlab-secrets.json
      1. [ ] /etc/gitlab/gitlab.rb
   1. [ ] reconfigure/restart and verify your restore has completed
      1. [ ] `sudo gitlab-ctl reconfigure`
      1. [ ] `sudo gitlab-ctl restart`
      1. [ ] `sudo gitlab-rake gitlab:check`


 - Congratulations!  You have successfully taken a backup of your instance and ran a restore!
 - If you have any questions, or run into any issues, please first check our [troubleshooting](https://docs.gitlab.com/ee/raketasks/backup_restore.html#troubleshooting) docs to see if you are able to correct any errors on your own. If you are still stuck, reach out to someone on the team who's already [experienced in Backup-Restore](https://gitlab-com.gitlab.io/support/team/skills-by-subject.html?search=Backup).


## Stage 3: Tickets

**This stage can be skipped with manager approval**

- [ ] **Done with Stage 3**

1. [ ] Find 2-3 *Solved* tickets to get a sense of how others have handled basic backup/restore issues
   1. [ ] __
   1. [ ] __
   1. [ ] __

1. [ ] Answer 2-3 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __



## Stage 4: Additional information/reading

- [ ] **Done with Stage 4**

1. [ ] Review our [troubleshooting docs](https://docs.gitlab.com/ee/raketasks/backup_restore.html#troubleshooting)
1. [ ] *If possible* comment any errors, or trouble, that you ran into while going through the backup/restore process

 - [Example Ticket 1](https://gitlab.zendesk.com/agent/tickets/325719)
 - [Example Ticket 2](https://gitlab.zendesk.com/agent/tickets/331074)
 - [Example Escalated Issue](https://gitlab.com/gitlab-com/geo-customers/-/issues/89)

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).


## Final Stage

1. [ ] Have your trainer review your tickets. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in Backup-Restore on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).


/label ~module
/label ~"Module::Backup-Restore"
/assign me