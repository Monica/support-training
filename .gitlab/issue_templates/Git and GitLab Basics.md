---
module-name: "Git and GitLab Basics"
area: "Core Technologies"
maintainers:
  - ralfaro
---

## Introduction

- We use Git and GitLab to build GitLab! Becoming familiar with the core tools is the first step to being an effective Support team member.
- We'll also introduce you to the GitLab product in this module.

**Goals of this checklist**

At the end of the checklist, you will:
- be equipped with the necessary technical baseline knowledge to understand our products and services.

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your Onboarding.
- This issue should take  **2 days to complete**.

## Stage 0: Git & GitLab Basics

If you are already comfortable with using Git, and you are able to retain a good amount of information by just watching or reading through, go for it! But if you see a topic that is completely new to you, stop the video and try it out for yourself before continuing.

1. [ ] (Shorter method -- 1 long webpage) Read [Learn the Basics of Git in Under 10 Minutes](https://www.freecodecamp.org/news/learn-the-basics-of-git-in-under-10-minutes-da548267cc91/)
1. [ ] (Longer method -- entire book) Explore [Git internals](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain) and go back to it from time to time to learn more about how Git works

1. [ ] Read about [GitLab releases](https://about.gitlab.com/handbook/engineering/releases/#overview-and-terminology)
1. [ ] Take a look at how the different GitLab versions compare
   1. [ ] [Feature Comparison list](https://about.gitlab.com/pricing/feature-comparison/)
   1. [ ] [Choosing between Self-managed and GitLab.com (SaaS)](https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed) | (Optional) [Video](https://www.youtube.com/watch?v=7GxO6qvATwU)
1. [ ] Complete the [GitLab Certified Git Associate Pathway](https://levelup.gitlab.com/learning-paths/certified-git-associate-learning-path) training 
1. [ ] Complete the [GitLab Certified Project Management Associate Pathway for GitLab Team Members](https://levelup.gitlab.com/learning-paths/gitlab-project-management-associate-learning-path) training focusing on how things are organized in GitLab

### Stage 1: GitLab Services & Product Stages

1. [ ] Get familiar with the [different teams in-charge of every stage in the DevOps cycle](https://about.gitlab.com/handbook/product/categories/#devops-stages) and for what they are responsible. This will help you add the right labels when creating issues and escalate in the right Slack channel.
1. [ ] Get familiar with the services GitLab offers on the [pricing page](https://about.gitlab.com/pricing/) with the Free, Premium, and Ultimate tiers. When a customer clicks a plan, they will be given the option of SaaS (GitLab.com) or Self-Managed.
**Note:** Be aware that [this three-tier subscription model was introduced in 2021-01-26](https://about.gitlab.com/blog/2021/01/26/new-gitlab-product-subscription-model/), and some customers are still on Bronze/Starter.

#### Congratulations! You made it, and now have a baseline knowledge of Git, GitLab and its services!

You are now ready to continue on your onboarding path to tackle the next module in line, check our [Support Training](https://handbook.gitlab.com/handbook/support/training/) page or your `New Support Team Member Start Here` issue for details!

If you think of any improvements to this module, please submit an MR! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).

/label ~onboarding
/label ~"Module::Git and GitLab Basics"
