---
module-name: "New Support Team Member Start Here"
area: "Customer Service"
maintainer:
  - rspainhower
---

## Introduction

Welcome to the Support Team, we are so excited that you've joined us!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://handbook.gitlab.com/handbook/support/workflows/working_with_security/#general-guidelines-1) is OK!**

**For the manager: Creating this Issue**

1. [ ] Create an issue using this template by making the Issue Title: <team member name> - New Support Team Member Start Here
1. [ ] Edit this issue's description to remove the irrelevant path based on the team member's role (engineer or manager).
1. [ ] Add the team member and their Onboarding Buddy as the assignees.
1. [ ] Set an initial due date 6 weeks from the start date.
1. [ ] (APAC only) Invite the team member to the monthly Skip Level with the Senior Support Engineering Manager.


**Goals of this Support Team Member Onboarding Issue**

This Issue is an tracking issue with a checklist of all the Learning Modules that comprise Support-specific Onboarding. Keep this issue open until you complete all the Learning Modules for your role.

**General Timeline and Expectations**

- Read about our [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/).
- Your [Support Onboarding Buddy](https://handbook.gitlab.com/handbook/support/training/onboarding_buddy/) is your primary contact-person during your onboarding. They will schedule regular check-ins with you and will be able to guide you through your [Onboarding Modules](https://handbook.gitlab.com/handbook/support/training/#support-onboarding-pathway) at least until your onboarding tasks have been completed. Once onboarding has finished, continued pairing with the Onboarding Buddy is always encouraged but at the support engineer's discretion. 


## Support Engineer onboarding pathway

Each of the modules listed below has an issue template in our [Support Training project](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates). To create your training issue, create a new issue using the links below, follow the instructions for the title, assign it to yourself, and save.

**NOTE:** When you instantiate one of the modules, replace the placeholder link below with a direct link to the new Issue ID.

It should take you **approximately 6 weeks** to complete all the modules that make up your onboarding pathway. It is expected that you will have more than one module open at any given time. For example, you will begin working on tickets partway through this process. Please refer to the [Support Onboarding process](https://handbook.gitlab.com/handbook/support/training/).

1. New Support Team Member Start Here (this issue)
1. [ ] Git & GitLab Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Git%20and%20GitLab%20Basics" target="_blank">create new issue</a>)
1. [ ] Customer Service Skills (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Customer%20Service%20Skills" target="_blank">create new issue</a>)
1. [ ] GitLab Support Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab%20Support%20Basics" target="_blank">create new issue</a>)
1. [ ] Zendesk Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Zendesk%20Basics" target="_blank">create new issue</a>)
1. [ ] Triaging Tickets (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Triaging%20Tickets" target="_blank">create new issue</a>)
1. Check off the following tasks when the modules are created. Completion is not required before closing this issue, and should be worked on in parallel and completed according to the expected timeline:
    1. [ ] Working on Tickets (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Working%20On%20Tickets" target="_blank">create new issue</a>)
    1. [ ] Documentation (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Documentation" target="_blank">create new issue</a>)
    1. [ ] Speak with your manager and open the relevant modules for the chosen [area of focus pathway](https://handbook.gitlab.com/handbook/support/training/#support-engineer-area-of-focus-pathway).

## Support Manager onboarding pathway

Each of the modules listed below has an issue template in our [Support Training project](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates). To create your training issue, create a new issue using the links below, follow the instructions for the title, assign it to yourself, and save.

**NOTE:** When you instantiate one of the modules, replace the placeholder link below with a direct link to the new Issue ID.

1. New Support Team Member Start Here (this issue)
1. [ ] Support Manager Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Support%20Manager%20Basics" target="_blank">create new issue</a>)
1. [ ] Git & GitLab Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Git%20and%20GitLab%20Basics" target="_blank">create new issue</a>)
1. [ ] GitLab Support Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab%20Support%20Basics" target="_blank">create new issue</a>)
1. [ ] Zendesk Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Zendesk%20Basics" target="_blank">create new issue</a>)
1. [ ] Triaging Tickets (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Triaging%20Tickets" target="_blank">create new issue</a>)
1. [ ] Customer Emergencies (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Customer%20Emergency%20On-Call" target="_blank">create new issue</a>)
1. [ ] GitLab.com CMOC (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab-com%20CMOC" target="_blank">create new issue</a>)
1. [ ] Booster Dotcom Emergency Training (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab-com%20Emergency%20Booster" target="_blank">create new issue</a>)
1. [ ] SSAT Reviewing Manager (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=SSAT%20Reviewing%20Manager" target="_blank">create new issue</a>)
1. Check off the following tasks when the modules are created. Completion is not required before closing this issue, and should be worked on in parallel and completed according to the expected timeline:
    1. [ ] Documentation (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Documentation" target="_blank">create new issue</a>)

As you will work closely with the Sales and Customer Success departments, it is important to understand their perspective. We created specific sales focused trainings called [Tanuki Tech](https://about.gitlab.com/handbook/marketing/sales-development/tanuki-tech/#tanuki-tech) with introductions to tech and gitlab in general. They are also a good starting point for a Support Manager to get a big picture view of the company. Take a look at the trainings below to find out more:
1. [ ] [Introduction to Technology](https://levelup.gitlab.com/courses/tt100-introduction-to-technology) teaches fundamental concepts such as applications, operations, and the hybrid cloud.
1. [ ] [Introduction to Gitlab](https://levelup.gitlab.com/courses/tt101-introduction-to-gitlab) helps you to understand `What does GitLab do?` (at 5:06) and `Where we are in product evolution` (at 9:35).
1. [ ] The [People of Technology](https://levelup.gitlab.com/courses/tt102-people-of-technology) course helps you get a better understanding on the different technical roles at the company.
1. [ ] In [Introduction to Development](https://levelup.gitlab.com/courses/tt200-introduction-to-development) topics include how software is developed, where applications run, the public cloud, and APIs.


Once you complete the onboarding modules, you may also wish to complete "Installation & Administration Basics"(<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=GitLab%20Installation%20and%20Administration%20Basics" target="_blank">create new issue</a>)  and "Customer Service Skills"(<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Customer%20Service%20Skills" target="_blank">create new issue</a>) from the Support Engineer onboarding pathway.


## Final Steps, Onboarding Feedback & Documentation

Keeping our documentation and workflows up to date will ensure that all Support team members will be able to access and learn from the best practices of the past. Giving feedback about your onboarding experience will ensure that this document is always up to date, and those coming after you will have an easier time coming in.

1. [ ] Schedule a call with your manager to discuss your onboarding issue or integrate this into your 1:1. Answer the following questions:
    - What was most helpful?
    - What do you wish existed in your onboarding, but does not?
    - In which areas do you still not feel confident?
    - In which areas do you feel strong?
  1. [ ] Make an update to one or more Support training module templates to make it better and link them below. The files are located in an issue template in the [support-training repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates). Have [a maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).
      1. \_________
1. [ ] Set up your [Calendly account](https://handbook.gitlab.com/handbook/support/workflows/calendly/)
1. [ ] Read about Gmail [labels](https://support.google.com/mail/answer/118708?hl=en&co=GENIE.Platform%3DDesktop) and how you can optimise your email inbox for better efficiency in our [Handbook](https://about.gitlab.com/handbook/tools-and-tips/#filters). Check for [Search operators](https://support.google.com/mail/answer/7190?hl=en) and how you can use them in your [filter rules](https://support.google.com/mail/answer/6579?hl=en#zippy=%2Ccreate-a-filter)
1. [ ] Consider joining the [#spt_donut](https://gitlab.slack.com/archives/C01AL6L262J) Slack channel if you want donut pairing with support team members.
1. [ ] **APAC only**: Consider joining the [#spt_donut-apac](https://gitlab.slack.com/archives/C03Q7AB8RUH) Slack channel if you want donut pairing with APAC support team members.
1. [ ] **APAC and EMEA only:** Discuss with your manager about adding yourself to the [Support Team Sync meeting rotation as Chair and Note-taker](https://docs.google.com/document/d/1jwj5g0BIq3kTepw2-ZD9VSETs7Isf6YDHGzmYxmTt50/edit#heading=h.tyva1uqh3g20).


#### Congratulations on completing your Support Onboarding modules successfully!

/label ~onboarding
/label ~"Module::New Support Team Member Start Here"
