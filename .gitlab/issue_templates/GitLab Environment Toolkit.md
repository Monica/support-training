---
module-name: "GitLab Environment Toolkit"
area: "Product Knowledge"
gitlab-group: "Quality"
maintainers:
  - amulvany
---

## Overview

**Goal:** Set a clear path for GitLab Environment Toolkit expert training

**Objectives:**
The GitLab Environment Toolkit automates the deployment of our reference architectures.
Debugging an existing deployment will not vary greatly from a reference architecture that has been deployed manually.
Therefore our primary objective with this module is to understand enough about the GitLab Environment toolkit that
we are able to debug any issues which customers may encounter during deployments, upgrades, recoveries and the like.
Once completed, one could be considered a "reference architecture expert" as well.  This is because you will soon understand all of the
components and services that are deployed by the GitLab Environment Toolkit.

The GitLab Environment Toolkit is a powerful tool for support use as well and will allow us to deploy multi-node features such as
GitLab Geo or Gitaly Cluster in a uniform manner which will minimize human error and without the time burden of having to follow each
and every step of the documentation.

To achieve our overall objective of becoming a GitLab Environment Toolkit expert we will need to:
- Understand the components of GitLab reference architectures
- Know how to use Hackystack to create your cloud sandbox
- Know how to deploy the infrastructure with Terraform
- Know how to deploy GitLab and then configure using Ansible
- Deploy a 1000 user architecture via the GitLab Environment Toolkit then terminate all services
- Understand how to minimize running costs when working with the GitLab Environment Toolkit

---

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: GET - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical GET questions to you.
1. [ ] Request access to the [`@gitlab-com/support/focus/get` group](https://gitlab.com/gitlab-com/support/focus/get) by pinging a direct member of the group. This is used to call attention to GET project issues which Support team members might be interested in. To see the current direct members, filter the group by "Member = Direct" or [use this link](https://gitlab.com/groups/gitlab-com/support/focus/get/-/group_members?with_inherited_permissions=exclude)
1. [ ] Optional: In your Slack Notification Settings, set **GET** as a [Keyword Notification](https://slack.com/help/articles/201355156-Configure-your-Slack-notifications#keyword-notifications).
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you've started learning this knowledge area:
   
   ```
   knowledge_areas:
   - name: GitLab Environment Toolkit
     level: 1
   ```

## Stage 1: Become familiar with the GitLab Environment Toolkit and Sandbox Cloud Realm

- [ ] **Done with Stage 1**

1. [ ] Learn about the Sandbox cloud realms.
    1. [ ] Read through [Sandbox Cloud Realm](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#overview)
1. [ ] Learn about the GitLab Environment Toolkit:
    1. [ ] Read through the [Reference Architectures](https://docs.gitlab.com/ee/administration/reference_architectures/) documentation.
           While reading this documentation try to understand why it would be important to automate the deployment of reference architectures.
    1. [ ] Skim through the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit) documentation.
    1. [ ] Take note of the video [GitLab Environment Toolkit: Up and running on AWS](https://www.youtube.com/watch?v=lAoeC3corQ0), this may help you if you get stuck during this training module.
    1. [ ] Read about [Terraform by HashiCorp](https://www.terraform.io/) and consider tackling the [Terraform training module]()
    1. [ ] Read [How Ansible Works](https://www.ansible.com/overview/how-ansible-works)
    1. [ ] Join the #gitlab_environment_toolkit channel on Slack.
    1. [ ] Optional:  Bookmark the [GET agenda Google doc](https://docs.google.com/document/d/10YBAutD4c1imsRrJBhmI8H2xbZOzHdOl5xywX_Zj22A) for weekly updates

## Stage 2: Build your infrastructure

- [ ] **Done with Stage 2**

**Note:** The GitLab Environment Toolkit supports GCP, AWS and Azure.  In order to minimize the number of problems one could encounter while working through this module we will select AWS as our platform. A later iteration may support all three.

1. [ ] Optional: Consider automatically provisioning a GET reference architecture (or several [1K](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit-configs/quality/-/tree/main/configs/reference_architectures/1k/terraform/environment.tf#L8-22), [3K](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit-configs/quality/-/tree/main/configs/reference_architectures/3k/terraform/environment.tf#L8-48), [10K](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit-configs/quality/-/tree/main/configs/reference_architectures/10k/terraform/environment.tf#L8-54) ) in GCP from [HackyStack](https://gitlab.com/gitlab-com/infra-standards/project-templates/support-Gitlab-Environment-Toolkit-template/-/blob/main/README.md) and inspect the created resources. You can also have a look at the the pipeline's artficats to see the GET project file structure that was used to deploy the reference architecture.
1. [ ] Create your AWS sandbox account and log in
    1. [ ] Complete the steps of [Sandbox Realm: How to get started](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#how-to-get-started)
    1. [ ] Using the IAM credentials from the previous step confirm that you can log into the [AWS Management Console](https://console.aws.amazon.com/iam/home)
1. [ ] Prepare Amazon Web Services
    1. [ ] Read through [Preparing the environment: Overview](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_prep.md#overview).
    1. [ ] Follow the steps in [Preparing the environment: Amazon Web Services (AWS)](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_prep.md#amazon-web-services-aws).  You will need your your access/secret keys and SSH key pair later, so save these locally.
           Even though we are working in a sandbox environment it is essential to keep your access/secret keys private.  Never commit these to a repository or display them on a screen share.  If you have any doubts then delete them immediately and repeat this step to generate new ones.
1. [ ] Follow [Provision your environment with Terraform](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_provision.md) and check off the following items along the way.
    1. [ ] Go ahead and [Install Terraform](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_provision.md#1-install-terraform) (and Ansible later).   You can install to your local machine, a local virtual machine or a cloud instance. Both Terraform and Ansible will run natively in Mac or Linux.
    1. [ ] Let's now [configure your environment](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_provision.md#2-setup-the-environments-config).
        1. [ ] Copy the Terraform example config files for a [10k AWS reference architecture](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit-configs/quality/-/tree/main/configs/reference_architectures/10k_aws/terraform) into the `terraform/environments/<env_name>` folder of your GET checkout as described in the docs.  We will start with this as a base.
        1. [ ] Modify this environment to minimize costs _prior_ to the provision stage.  To do this open `environment.tf` in a text editor. Change `*_node_count` values to 1 with the exception of `consul_node_count`, `redis cache`, `redis persistent` and `postgres_node_count` which require a minimum of 3. Change all instance types to [t3 burstable types](https://aws.amazon.com/ec2/instance-types/t3/).
    1. [ ] Run the command `terraform plan` the output below as `stage-2.json`
    1. [ ] Once you feel comfortable that everything is in place, go ahead and [provision your environment](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_provision.md#3-provision)

## Stage 3: Deploy GitLab with Ansible

- [ ] **Done with Stage 3**

    1. [ ] Follow the [install Ansible steps](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_configure.md#1-install-ansible) and install Ansible and dependencies to the same machine as Terraform
    1. [ ] Copy the Ansible example config files for a [10k AWS reference architecture](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit-configs/quality/-/tree/main/configs/reference_architectures/10k_aws/ansible/) into the  `ansible/environments/<env_name>` folder of your GET checkout. Then [setup your environments inventory and config](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_configure.md#2-setup-the-environments-inventory-and-config) using these files as a base.  You most likely need to change the region, prefixes and set your passwords for the different components. (If you're using the default AMI on the example, make sure the AWS zone in which you are building is `us-east-1`)
    1. [ ] Now we can [provision the infrastructure](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_configure.md#3-configure-update) using Ansible.

## Stage 4: Run the GitLab Performance Tool against the reference architecture

- [ ] **Done with Stage 4**

1. [ ] Follow the steps from the [GPT Quick Start](https://handbook.gitlab.com/handbook/support/workflows/gpt_quick_start/) page and perform a 1k user test against your architecture
    1. [ ] Share the [test summary](https://gitlab.com/gitlab-org/quality/performance/-/blob/main/docs/k6.md#test-output-and-results) in the output below.

## Stage 5: Zero downtime upgrades

- [ ] **Done with Stage 5**

**Note:** By the time you reach this stage a new version of GitLab should have been released.

1. [ ] Prepare for the upgrade
    1. [ ] Read [How upgrades work](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_upgrades.md#how-upgrades-work)
    1. [ ] Review the release notes as described in [Check for any Toolkit breaking or config changes](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_upgrades.md#check-for-any-toolkit-breaking-or-config-changes)
    1. [ ] Perform a [Terraform dry run](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_upgrades.md#perform-terraform-dry-runs-for-new-toolkit-versions)
    1. [ ] Perform a [zero downtime update](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit/-/blob/master/docs/environment_upgrades.md#zero-downtime-updates) to the latest major version

## Stage 6: Remove all resources

- [ ] **Done with Stage 6**

    1. [ ] Optional: Destroy the GET reference architecture automatically provisioned with [HackyStack](https://gitlab.com/gitlab-com/infra-standards/project-templates/support-Gitlab-Environment-Toolkit-template/-/blob/main/README.md) in GCP.
    1. [ ] Destroy all of your resources using [terraform destroy](https://www.terraform.io/docs/cli/commands/destroy.html)
    1. [ ] Using the console or API of Amazon Web Services confirm that the resources have been removed
    1. [ ] Run the command `terraform show -json` to print state and attach the JSON below as `stage-4.json`

## Stage 7: Quiz

- [ ] **Done with Stage 7**

1. [ ] Provide a link to the 10k_azure configuration
1. [ ] Can the toolkit deploy different versions of GitLab?
1. [ ] Share a summary of how one could configure backups for an environment deployed using the toolkit.
1. [ ] What could go wrong when using the GitLab Environment Toolkit?  Share a few hypotheticals of what a customer may encounter.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

## Final Stage:

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: GitLab Environment Toolkit
     level: 2
   ```

/label ~module
/label ~"Module::GitLab Environment Toolkit"
/assign me
