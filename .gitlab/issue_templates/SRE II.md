---
module-name: "SRE II"
area: "Product Knowledge"
gitlab-group: "Enablement:Infrastructure"
maintainers:
  - rhassanein
total-time: "~ 10 hours"
---

<!---
This Training Module was inspired by one SRE Internship:
https://gitlab.com/gitlab-com/people-group/Training/-/issues/779
--->

### Prerequisites

- [ ] [SRE Introduction](./SRE Introduction.md) module
- [ ] (Optional) [Kubernetes Part 1](./Kubernetes Part 1.md) module.


**Title:** _SRE Infrastructure II (Intermediate) - **your-name**"_

Preferably, follow the order of the stages, each stage prepares for the following one.

### Stage 1: Commit to learning about Site Reliability Engineering in depth (5 minutes)

- [ ] **Done with Stage 1**

1. [ ] Notify your manager on the issue to let them know them you have started.
1. [ ] Notify the team via one of the Support Channels.
1. [ ] (Optional) Read the first few chapters of [The SRE Book from Google](https://sre.google/sre-book/table-of-contents/) along with working this issue.

### Stage 2: Closer look at the Reliability Team (~1 hour)

This stage zooms into the Reliability sub-team structure and responsibilities.

While SREs from different divisions will call themselves just that (Site Reliability Engineer), this can be confusing as they actually perform different roles based on which team they belong to.

The Reliability team is particularly interesting because they have taken the sole responsibility of GitLab SaaS reliability. Team members from the Reliability team would typically spend ~25% of their monthly time oncall handling incidents, and ~25% performing RCA for ~S1 and ~S2 incidents, totaling ~50% of time handling incident-related work, whilst dedicating the rest of their time (~50%) to [Project work](https://about.gitlab.com/handbook/engineering/infrastructure/projects/).

Additionally, the Reliability team is the largest sub-team compared to Delivery and Scalability, which prompted the structure of the team to be divided even further into 3 sub-teams based on functionality: Observability, Datastore and CoreInfra.

- [ ] **Done with Stage 2**

  - [ ] Review each reliability sub-team section, responsibilities, KPIs and team members:
    1. [ ] The [Reliability](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/) team page.
    1. [ ] [Infrastructure KPIs](https://about.gitlab.com/handbook/engineering/infrastructure/performance-indicators/).
    1. [ ] Checkout each sub-team's Backlog and Sprint boards linked under [How We Work](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/#how-we-work).


### Stage 3: Gitlab.com Architecture (~2.5 hours)

Having a general knowledge of GitLab Production Architecture is a necessary context to participating in incidents. Below are
a few links that you're advised to spend as much time as you need to develop an initial understanding of the underlying architecture of GitLab.com. 

- [ ] **Done with Stage 3**

  1. [ ] Checkout the [Production Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture) page.
  1. [ ] Pay close attention to the [Monitoring architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/#monitoring-architecture). Monitoring is **critical** to ensuring a reliable system, having a general understanding of where to find information related to the health of GitLab.com and it's different components is vital for a prompt incident response.
  1. [ ] Read the [GitLab.com on Kubernetes](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/#gitlab-com-on-kubernetes) section in detail, visit the [related Epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112) and try to draw a picture of the complexity and challenges of the migration to K8S.
  1. [ ] Feel free to complete the training module [Introduction to GitLab Architecture](./Introduction to GitLab Architecture.md) by Weimeng - at the time of this writing it's [drafted here](https://gitlab.com/gitlab-com/support/support-training/-/merge_requests/608).

### Stage 4: Gitlab.com Observability (~1.5 hours)

The [observability team's](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/observability/) work is a key pillar during an incident's lifecycle, as identifying the cause and impact of an incident could only be figured through the logs and metrics the team builds. In this section, we'll spend sometime observing (_pun intended_) the related systems.

- [ ] **Done with Stage 4**

  1. [ ] Skim through [Official GitLab Handbook Page](https://about.gitlab.com/handbook/engineering/monitoring/) for all monitoring info.
  1.  [ ] Logs destination 1, [Kibana](https://log.gprd.gitlab.net/app/kibana)
       - [ ] If you haven't already, check the [Support Kibana Workflow](https://handbook.gitlab.com/handbook/support/workflows/kibana/) for some example queries you can try.
  1.  [ ] Logs destination 2, [Thanos](https://thanos-query.ops.gitlab.net/graph) and [Prometheus](https://prometheus.gprd.gitlab.net/graph?g0.expr=&g0.tab=1&g0.stacked=0&g0.range_input=1h), also checkout [Prometheus the movie](https://www.imdb.com/title/tt1446714/).
  1.  [ ] [Grafana](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s), a visual representation of our systems' health.
       - [ ] Checkout out the first section in [this page](https://about.gitlab.com/handbook/engineering/monitoring/#gitlabcom-service-level-availability).
       - [ ] Understand what an Apdex Score and Error rate is in general.
       - [ ] Understand how we implemented these indicators in the context of measuring Gitlab.com's health.
  1. [ ] Bug impacts on users, [Sentry](https://sentry.gitlab.net/gitlab/gitlabcom/).

### Stage 5: Past Incidents review (~2.5 hours)

Before attending live incidents, let's have a feel of what an incident issue looks like.

- [ ] **Done with Stage 5**

  1. [ ] Checkout one of the recorded videos of the [Infrastructure Fire Drills](https://about.gitlab.com/handbook/engineering/infrastructure/team/#infrastructure-fire-drill). The [full Youtube playlist can be found here](https://www.youtube.com/playlist?list=PL05JrBw4t0KoIaWOHVVBWGRUnMCgWrLzh).
  1. [ ] Study one past incident in detail, here's a [link to all incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?label_name%5B%5D=incident).
      - [ ] **TIP**: If you're not sure what to pick, checkout the incidents in this [example internship](https://gitlab.com/gitlab-com/people-group/Training/-/issues/779).
  1. [ ] Leave a link in the comments to the incident you reviewed and what your key takeaways are.

### Stage 6: GitLab.com Live Incidents! (~2.5 hours - ~)

You're ready! The best way to learn more about [incidents](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/) is to attend them!

- [ ] **Done with Stage 6** 

  1. [ ] If you haven't already, subscribe to the #incident-management slack channel.
  1. [ ] Keep an eye when a [~severity::1](https://gitlab.com/gitlab-com/gl-infra/production/-/labels?utf8=%E2%9C%93&subscribed=&search=severity%3A%3A1) or [~severity::2](https://gitlab.com/gitlab-com/gl-infra/production/-/labels?utf8=%E2%9C%93&subscribed=&search=severity%3A%3A2) incidents are declared, this is when the fun begins; you should be able to see the full investigation in real time via the Zoom room link attached to the incident, as well as the dedicated slack channel that will be auto-created.
  1. [ ] Attend at least 3 incidents, link them in the comment section below.
  1. [ ] Leave a detailed description in the comments of what you learned and your key takeaways (technical or otherwise!) from at least 1 of the incidents you attended.
  1. [ ] (Optional) Discuss what you learned in the [Weekly Support Call](https://handbook.gitlab.com/handbook/support/#weekly-meetings).

### Final Stage

- [ ] Your manager needs to check this box to acknowledge that you have finished.

/label ~module
/label ~"Module::SRE II"
