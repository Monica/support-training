---
module-name: "vagrant"
area: "Troubleshooting & Diagnostics"
gitlab-group: "Enablement:Distribution Group"
maintainers:
- [astrachan]
---

### Overview

**Goal**: Understand what **Vagrant** is and how GitLab Support uses this tool to explore GitLab installation setups.

*Length*: 8 hours

**Objectives**: At the end of this module, you should be familiar with:

- Where to download **Vagrant**.
- How to install on MacOS, Linux.
- Which GitLab Support Toolbox projects use Vagrant.
- How this tool can enable GitLab Support Engineers to explore (replicate) different GitLab setups.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: Vagrant - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts so that they have a heads up that you are undertaking the module.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Become familiar with what Vagrant is

- [ ] **Done with Stage 1**

1. [ ] Visit [Vagrant Documentation](https://www.vagrantup.com/docs)
1. [ ] Install [Vagrant](https://www.vagrantup.com/docs/installation)
1. [ ] Complete [vagrant Quick Start](https://learn.hashicorp.com/tutorials/vagrant/getting-started-index?in=vagrant/getting-started)

## Stage 2: GitLab Support uses Vagrant in [GitLab Support Toolkit](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-toolkit)

> Support toolkit to help manage variable GitLab inventory, reproduce issues across a wide variety of environments and perform log analysis.

- [ ] **Done with Stage 2**

1. [ ] Complete [gitlab-support-toolkit#quick-start](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-toolkit#quick-start)
1. [ ] Review [gitlab-support-toolkit#additional-services](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-toolkit#additional-services) to read how this project can be used to automatically install Docker service within the VM provisioned by Vagrant.

## Stage 3: GitLab Support uses Vagrant in [GitLab Support Setups](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-setups)

> Provide a common provisioning and directory structure for various support setups.
The setups expand from single to multi-host, including integration and external
resources.

- [ ] **Done with Stage 3**

1. [ ] Complete [Getting Started](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-setups#getting-started)
1. [ ] Complete [GitLab with GitLab-Runner v13](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-setups/-/tree/master/docs/setups/gitlab_runner_v13)
1. [ ] Review [gitlab_runner_v13/Vagrantfile](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-setups/-/blob/master/setups/vagrant/gitlab_runner_v13/Vagrantfile)
1. [ ] Review
   * [Configuration for GitLab and GitLab-Runner versions](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-setups/-/blob/master/inventories/vagrant/group_vars/gitlab_runner_v13.yml)
   * [GitLab-Runner Configuration](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-setups/-/blob/master/inventories/vagrant/host_vars/gss-vm-runner-v13-01.vagrant.yml)
1. [ ] Complete [openldap - OpenLDAP](https://gitlab.com/gitlab-com/support/toolbox/gitlab-support-setups/-/blob/master/docs/setups/openldap/openldap.md)

### Penultimate Stage: Review

You feel confident you've achieved all of the objectives, and now know:

- [ ] Where to download **Vagrant**
- [ ] How to install on MacOS, Linux.
- [ ] Which GitLab Support Toolbox projects use Vagrant
- [ ] How this tool can enable GitLab Support Engineers to explore (replicate) different GitLab setups.

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your trainer review this issue. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in Vagrant on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module
/label ~"Module::Vagrant"
/assign me
