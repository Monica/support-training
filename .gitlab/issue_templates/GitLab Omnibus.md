---
module-name: "GitLab Omnibus"
area: "Product Knowledge"
gitlab-group: "Enablement:Distribution"
maintainers:
  - TBD
---

## Overview

**Goal:** Set a clear path for Omnibus Expert training

**Objectives:**

- Know how to install the latest version GitLab via Omnibus.
- Know how to install a specific version of GitLab via Omnibus.
- Know how to both upgrade and downgrade an Omnibus install of GitLab.
- Know how to both backup and restore an Omnibus install of GitLab.
- Know how to reset a user's password via the gitlab-rails console.
- Know how to apply a patch to an Omnibus install of GitLab.
- Be able to locate the logs that detail the previous reconfigures that were
  run.

---

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: Omnibus - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Omnibus questions to you.
1. [ ] In your Slack Notification Settings, set **Omnibus** and **Upgrade** as [Keyword Notifications](https://slack.com/help/articles/201355156-Configure-your-Slack-notifications#keyword-notifications)
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you've started learning this knowledge area:
   
   ```
   knowledge_areas:
   - name: Omnibus
     level: 1
   ```

## Stage 1: Become familiar with what Omnibus is

- [ ] **Done with Stage 1**

1. [ ] Learn about Omnibus:
    1. [ ] Read through [Omnibus GitLab Docs](https://docs.gitlab.com/omnibus/)
    1. [ ] Familiarize yourself with [GitLab Installation Methods](https://about.gitlab.com/install/)
    1. [ ] Watch Alex's video about the [GitLab Omnibus](https://youtu.be/dqImFA11dtk)
    1. [ ] Read through [Manually Downloading and Installing a GitLab Package](https://docs.gitlab.com/omnibus/manual_install.html)
    1. [ ] Read through [Backing up and restoring GitLab](https://docs.gitlab.com/ee/raketasks/backup_restore.html)
    1. [ ] Read through [Updating GitLab installed with the Omnibus GitLab package](https://docs.gitlab.com/omnibus/update/)
    1. [ ] Read through [Upgrade recommendations](https://docs.gitlab.com/ee/policy/maintenance.html#upgrade-recommendations)
    1. [ ] Read through [Downgrading](https://docs.gitlab.com/ee/update/package/downgrade.html)
    1. [ ] Read through [Upgrade packaged PostgreSQL server](https://docs.gitlab.com/omnibus/settings/database.html#upgrade-packaged-postgresql-server)
    1. [ ] Read through [Patching an instance](https://handbook.gitlab.com/handbook/support/workflows/patching_an_instance/)
    1. [ ] Read about the [GitLab log system](https://docs.gitlab.com/omnibus/settings/logs.html).
    1. [ ] Read about [verifying package integrity](https://docs.gitlab.com/omnibus/update/package_signatures.html#manual-verification)

## Stage 2: Technical setup

- [ ] **Done with Stage 2**

1. [ ] Follow [GitLab Installation Methods](https://about.gitlab.com/install/)
       to install the latest version of GitLab on a VM.
1. [ ] Follow [Manually Downloading and Installing a GitLab Package](https://docs.gitlab.com/omnibus/manual_install.html)
       to install GitLab version 11.3.4 on a VM.
1. [ ] Successfully create a backup of a running GitLab instance including configuration files.
1. [ ] Perform a restore using the backup you created.
1. [ ] Practice upgrading and downgrading your instance.
    -  [ ] Perform at least one upgrade on a Debian-based system (Debian, Ubuntu, etc). Use the package manager to perform a dry run or a simulation of the upgrade.
    -  [ ] Perform at least one upgrade on an RPM-based system (CentOS, Fedora, etc). Use the package manager to perform a dry run or a simulation of the upgrade.
1. [ ] Verify that you are running the intended version of GitLab. There are multiple ways to do this. Leave a comment with the way(s) you checked the version of GitLab.
1. [ ] Practice [setting the root password](https://docs.gitlab.com/omnibus/settings/configuration.html#set-initial-root-password-on-installation) during installation
1. [ ] Using a `.deb` file you downloaded from [packagecloud](https://packages.gitlab.com/gitlab/), perform the steps necessary to [manually verify the integrity](https://docs.gitlab.com/omnibus/update/package_signatures.html#manual-verification) of the package. Add a comment below showing the results.
1. [ ] Review the Omnibus-included `gitlab-ctl` commands and Rake tasks.
```bash
# gitlab-ctl commands
gitlab-ctl --help
# gitlab-rake options
gitlab-rake --help
# gitlab-rake tasks
gitlab-rake --tasks
```

## Stage 3: Tickets

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Quiz

- [ ] **Done with Stage 4**

1. [ ] Schedule a call with a current Omnibus trainer. During this call, you
        will guide them through the following:
    1. [ ] Install GitLab version 11.9.9 via Omnibus. (Avoid specifying an `EXTERNAL_URL` with https; see [this issue](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/4614) for more information.)
    1. [ ] Upgrading the GitLab installation to version 12.0.0, telling it to
            skip the forced postgres upgrade. Make sure to follow the [upgrade recommendations](https://docs.gitlab.com/ee/policy/maintenance.html#upgrade-recommendations)!
    1. [ ] Downgrade the GitLab version to version 11.11.8.
    1. [ ] Performing a restore on the GitLab instance using `1611272835_2021_01_21_11.11.8-ee_gitlab_backup.tar `
            and `gitlab-secrets.json` from the [content/omnibus](https://gitlab.com/gitlab-com/support/support-training/tree/master/content/omnibus) directory in this project
    1. [ ] Apply `example.patch` from [module-omnibus - content](https://gitlab.com/gitlab-com/support/support-training/tree/master/content/omnibus)
            to your instance.
    - **Note:** Be sure to reference or show any links that you looked up at each
    step of the quiz
1. [ ] Once you have completed this, have the trainer comment below acknowledging your success.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

## Final Stage:

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: Omnibus
     level: 2
   ```

/label ~module
/label ~"Module::GitLab Omnibus"
/assign me