---
module-name: "Support Hiring - Stage1 Assessment Review Training"
area: "Customer Service"
maintainers:
  - lyle
---

So - you want to help in the hiring process! That's awesome. You (or your manager)
should open one of these issues shortly after you complete your onboarding. There are 2 different interview stages you can contribute to.  This is the first of them - the Take Home Assessment.  This doesn't involve direct interaction with the candidate. Not _everyone_ has
to proceed to learning to do the technical interviews, but we definitely want everyone to be involved reviewing assessments.

- [ ] Add your manager as an assignee in this issue.

### Take Home Assessment Review
- [ ] Review the assessment questions and answers in our [hiring process](https://gitlab.com/gitlab-com/people-group/hiring-processes/tree/master/Engineering/Support). 
- [ ] Review the [guidelines](https://gitlab.com/gitlab-com/support/support-assessment-solutions/-/blob/master/README.md) for reviewing the take home assessments.
- [ ] Open an MR to add your name to the list of individuals in training in the [interview process document](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md#2-technical-assessment).
- [ ] The Recruiter will add you to the Assessment Graders list and will assign the next two Assessments to you. You will receive an email notification when the Assessments come in. You will be tagged on a candidate's profile to pair up with another Grader who might be located in or near your region. Pair with them while they review Assessments and talk through what sticks out to them. Feel free to reach out in `#spt_hiring` in case you have any questions.
- [ ] When the third Assessment comes in, reverse pair with a Grader in your region on an Assessment review. You will be tagged in on a candidate's profile again. Talk the Grader through what sticks out to you. 
- [ ] _Optional_ For passing candidates, in a month or so since grading, connect with the recruiter to follow up on the candidate’s process and reflect:
   - Were they hired?
   - Were any of the notes you provided covered by future interviews?
   - Is there anything you need to be sensitive in for future reviews?
- [ ] Make a merge request on the [Support Engineering Interview Process](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md) to add yourself to the **Assessment** section and assign it to your manager. Make sure to ping [your region's technical recruiter](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md#peopleops-hiring-team) in the MR so that they are informed.
- [ ] [Create an Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#slack-google-groups-1password-vaults-or-groups-access-requests) to add yourself to `@support-assessment-graders` Slack group. 


/label ~module ~"interview training"
/label ~"Module::Support Hiring - Stage1 Assessment review training"
