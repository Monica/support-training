---
module-name: "Promotion - Job Title updates"
area: "tbd"
maintainers:
  - jgianoutsos
---

## Overview

**Goal**: Update your job title after being promoted. 

This issue lists the various places you will need to update your job title after being promoted within the support team. 

Set yourself as the assignee of this issue.

Note: Workday will have been updated as part of the promotion process, you do not need to action anything in Workday.

### 1. Updates in GitLab

* **[Support Team yaml](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml)**
   * [ ] Update `title` 
   * [ ] If your promotion involves a change of manager, update `reports_to` to your new manager
   * **Promotion to manager** : If your promotion is from an IC role to a Manager role:
      * [ ] update `Zendesk | Main | Groups` to add `- Support Managers`
      * [ ] update `Zendesk | Main | Role` to `Support Managers`
      * NOTE: If you are working USFed in your promoted role, the above changes will need to be done in the `Zendesk | us-federal` section
* [ ] Update [team page entry](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) in the team yaml file. Instructions in [the handbook](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page). Assign the Merge Request to your manager for merging.
* [ ] Update your role title in your [GitLab user profile](https://gitlab.com/-/profile)
* [ ] [at] mention your manager in a comment and ask them to add you as owner in the appropriate group, such as [Support Senior Engineers group](https://gitlab.com/groups/gitlab-com/support/senior-support-engineers/-/group_members?with_inherited_permissions=exclude) or [Support Managers](https://gitlab.com/groups/gitlab-com/support/managers/-/group_members?with_inherited_permissions=exclude).

### 2. Updates in other work tools

   * **Slack**
      * [ ] Update your [Slack profile](https://gitlab.slack.com/account/profile) to include your new role
      * [ ] Join now relevant Slack channels, such as the [#spt_seniors_plus](https://gitlab.slack.com/archives/C03F4JETMQX) slack channel.
   * **Gmail**
      * [ ] Update your Gmail signature to include your new role. [Here is an example you can use](https://handbook.gitlab.com/handbook/tools-and-tips/#sts=Email%20signature).
   * **Zoom**: Edit your [Zoom profile](https://zoom.us/profile). Guidance for formatting is in the [handbook](https://handbook.gitlab.com/handbook/support/#zoom).
      * [ ] update your `Job Title` 
      * [ ] update your display name with your new role
   * **Calendly**
      * [ ] there isn't a job title field, however some people include their title in other fields

### Other things to think about

* Consider any public profiles you have elsewhere such as GitLab Forum, LinkedIn, StackOverflow... 







/label ~module


