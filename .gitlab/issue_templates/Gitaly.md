---
module-name: "Gitaly"
area: "Product Knowledge"
gitlab-group: "Create:Gitaly"
maintainers:
  - bcarranza
  - ulisesf
---

## Introduction

This training module is intended to provide Support Engineers with a better understanding of Gitaly.

**Goals of this training module**

At the end of this module, you should be able to:
- answer Gitaly-related tickets (or have a good idea of how to start troubleshooting Gitaly-related tickets/problems)
- demonstrate an understanding of the role Gitaly plays in GitLab
- support peers with questions about Gitaly

**General Timeline and Expectations**

- This issue should take you **TBD to complete**.

Reminders:

- This is a **Public** issue; don't include anything confidential.
- You should do stage **0** first and the final stage last. Outside of that, you can perform tasks from the stages in any order you like.

### Stage 0: Create Your module

1. [ ] Create an issue using this template by making the Issue Title: "Learning Gitaly - <your name>"
1. [ ] Add yourself as the assignee
1. [ ] Consider setting a milestone and/or a due date to help motivate yourself!
1. [ ] Setup a [3k reference architecture ](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html). This environment will be needed for **Stage 2**.
1. [ ] Alternatively if you're versed with [GitLab Environment Toolkit (GET)](https://gitlab.com/gitlab-org/gitlab-environment-toolkit) use it  to deploy the [3k reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html) 

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

### Stage 0.5: Fundamental Concepts

- [ ] **Done with Stage 0.5**

> Optional.

**Goal** This stage is intended to help ensure that  people completing the module have a familiarity with fundamental concepts that are key to understanding Gitaly and the role it plays.

1. [ ] [gRPC](https://grpc.io/docs/what-is-grpc/introduction/)
1. [ ] [RPC](https://en.wikipedia.org/wiki/Remote_procedure_call)
1. [ ] [Rugged (legacy)](https://github.com/libgit2/rugged/blob/master/README.md)


### Stage 1. Gitaly in theory

- [ ] **Done with Stage 1**

**Goal** Understand the basics of what Gitaly does and how it fits into the GitLab application
1. [ ] Review the [GitLab architecture diagram](https://docs.gitlab.com/ee/development/architecture.html#simplified-component-overview), pay attention to where Gitaly sits in relation to other GitLab components
1. [ ] Read [The road to Gitaly v1.0](https://about.gitlab.com/blog/2018/09/12/the-road-to-gitaly-1-0/) to understand the problems that Gitaly was introduced to solve
1. [ ] Read the [Gitaly](https://docs.gitlab.com/ee/administration/gitaly/) docs for administrators
1. [ ] Review [Gitaly Basics slides](https://docs.google.com/presentation/d/1cLslUbXVkniOaeJ-r3s5AYF0kQep8VeNfvs0XSGrpA0/edit#slide=id.g1f2b47df5a_0_76)
1. [ ] Read about [Gitaly cluster](https://docs.gitlab.com/ee/administration/gitaly/praefect.html)
1. [ ] Read the [Gitaly Cluster design documents](https://gitlab.com/gitlab-org/gitaly/-/blob/master/doc/design_ha.md)
1. [ ] Watch [GitLab Support Gitaly Cluster Deep Dive](https://www.youtube.com/watch?v=NJDiXlahn9U)
1. [ ] Read the [Gitaly developers guide](https://docs.gitlab.com/ee/development/gitaly.html#deep-dive)
1. [ ] Read the [beginner's guide to Gitaly contributions](https://gitlab.com/gitlab-org/gitaly/-/blob/master/doc/beginners_guide.md)
1. [ ] Watch [Using `strace` to understand GitLab](https://www.youtube.com/watch?v=dgJH4wpR5OE) (~40 minutes)
1. [ ] Watch the [Gitaly for Ruby devs](https://www.youtube.com/watch?v=BmlEWFS8ORo) Deep Dive from May 2019 (~40 minutes)
1. [ ] Optional but recommended: watch the [How Gitaly fits into GitLab](https://www.youtube.com/playlist?list=PL05JrBw4t0KqoFUiX42JG7BAc7pipMBAy) playlist
   1. Watch [How Gitaly fits into GitLab: Episode 1 – Gitaly client](https://www.youtube.com/watch?v=j0HNiKCnLTI&list=PL05JrBw4t0KqoFUiX42JG7BAc7pipMBAy&index=1) (~56 minutes)
   1. Watch [How Gitaly fits into GitLab: Episode 2 - Git SSH](https://www.youtube.com/watch?v=0kY0HPFn25o&list=PL05JrBw4t0KqoFUiX42JG7BAc7pipMBAy&index=2) (~55 minutes)
   1. Watch [How Gitaly fits into GitLab: Episode 3 - Git push](https://www.youtube.com/watch?v=-kXYycFYDzo&list=PL05JrBw4t0KqoFUiX42JG7BAc7pipMBAy&index=3) (~58 minutes)
   1. Watch [How Gitaly fits into GitLab: Episode 4 - Git HTTP](https://www.youtube.com/watch?v=lM13p8lCu8A&list=PL05JrBw4t0KqoFUiX42JG7BAc7pipMBAy&index=4) - (~58 minutes)
   1. Watch [How Gitaly fits into GitLab: Episode 5 - Merge Requests across Forks](Fhttps://www.youtube.com/watch?v=yGSuOz0XOHQ&list=PL05JrBw4t0KqoFUiX42JG7BAc7pipMBAy&index=5) (~64 minutes)
   1. Watch [How Gitaly fits into GitLab: Episode 6 - Creating Git commits on behalf of Git users](https://www.youtube.com/watch?v=Rbe0KGTLkxY&list=PL05JrBw4t0KqoFUiX42JG7BAc7pipMBAy&index=6) (~68 minutes)
1. [ ] Read about [CVE-2020-13353](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-13353), a security vulnerability impacting Gitaly and take a look at the fix that was implemented. A brief summary of the vulnerability:

> When importing repos via URL, one time use git credentials were persisted beyond the expected time windows in Gitaly 1.79.0 or above.


### Stage 2. Gitaly in practice

- [ ] **Done with Stage 2**

**Goal** Get hands on with Gitaly operations

In **Stage 0**, if you used GET to setup the [3k reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures/3k_users.html), proceed to use that environment for the tasks in this stage. Otherwise use your manually created Gitaly cluster.

1. [ ] Run the `gitlab:gitaly:check` Rake task, familiarize yourself with the output.
1. [ ] Use the API to [schedule a repository storage move for a project](https://docs.gitlab.com/ee/api/project_repository_storage_moves.html#schedule-a-repository-storage-move-for-a-project) between shards in the environment you spun up using **GET**
1. [ ] Observe the impact of Gitaly timeouts
    - [ ] Create an intentionally large commit (1GB or so)
    - [ ] Decrease the [Gitaly timeouts](https://docs.gitlab.com/ee/user/admin_area/settings/gitaly_timeouts.html#available-timeouts)
    - [ ] Observe that the page fails to load
    - [ ] Increase the [Gitaly timeouts](https://docs.gitlab.com/ee/user/admin_area/settings/gitaly_timeouts.html#available-timeouts)
        - [ ] If necessary, increase `puma['worker_timeout']` to be at least 3 seconds longer than the Gitaly timeout and reconfigure.
        - [ ] If using Puma, `GITLAB_RAILS_RACK_TIMEOUT` must also be set to a value greater than your Gitaly time out [using gitlab_rails['env']](https://docs.gitlab.com/omnibus/settings/puma.html#worker-timeout)
    - [ ] Observe that the page loads successfully
1. [ ] Follow the Gitaly [authentication token rotation process](https://docs.gitlab.com/ee/administration/gitaly/configure_gitaly.html#rotate-gitaly-authentication-token), document the process, put notes about what you did, what you encountered, etc below.


### Stage 3. Gitaly at GitLab and beyond

- [ ] **Done with Stage 3**

**Goal** Learn about the current status and future direction of Gitaly at GitLab

1. [ ] Review the [Gitaly category vision](https://gitlab.com/groups/gitlab-org/-/epics/710) epic and associated issues that you find interesting
1. [ ] Familiarize yourself with the [Gitaly runbooks](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/gitaly) used for emergencies on `gitlab.com`
1. [ ] Review the March 2021 GitLab Support [Deep Dive on Gitaly Cluster](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3209) issue
1. [ ] Watch [one of the recordings](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3209#recording) from the March 2021 GitLab Support Deep Dive on Gitaly Cluster
  1. [ ] [Troubleshooting Gitaly High CPU and memory](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/gitaly/git-high-cpu-and-memory-usage.md)
  1. [ ] [Gitaly error rate is too high](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/gitaly/gitaly-error-rate.md)
  1. [ ] [Gitaly Queueing](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/gitaly/gitaly-rate-limiting.md)
1. [ ] Consider scheduling a coffee chat with someone from the [Gitaly group](https://about.gitlab.com/handbook/product/categories/#gitaly-group)
1.  [ ] Read the [Category Direction page for Gitaly](https://about.gitlab.com/direction/gitaly/)
1. [ ] Read [Gitaly at Scalingo: Explaining the complete redesign of how we host your application git repositories](https://dev.to/etiennem/gitaly-at-scalingo-explaining-the-complete-redesign-of-how-we-host-your-application-git-repositories-4550)
1. [ ] Read [How a fix in Go 1.9 sped up our Gitaly service by 30x](https://about.gitlab.com/blog/2018/01/23/how-a-fix-in-go-19-sped-up-our-gitaly-service-by-30x/)
1. [ ] _Optional_ Review GitLab Forum [posts about Gitaly](https://forum.gitlab.com/search?q=gitaly%20order%3Alatest) and provide an answer if possible.


##### :clipboard: Interesting Issues

1.  [ ] Review each of the [issues called out on the Category Direction page for Gitaly](https://about.gitlab.com/direction/gitaly/#top-customer-successsales-issues). Choose a few that are interesting to you, record them here and enable Notifications so you can follow the progress of the issue.
1. [ ] Browse through the [issues in the **gitlab-org/gitaly**](https://gitlab.com/gitlab-org/gitaly/-/issues) repository. Choose a few that are interesting to you, record them here and enable Notifications so you can follow the progress of the issue.

###### Issue List

1. []()
1. []()
1. []()
1. []()

#### Gitaly and NFS
1. [ ] Review the [Remove Gitaly support for NFS](https://gitlab.com/groups/gitlab-org/-/epics/3371) epic and associated issues
1. [ ] Review [GitLab 14.0 Support Readiness - Removal of support for Gitaly and NFS](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3435)
1. [ ] Read about [Gitaly and NFS](https://about.gitlab.com/support/statement-of-support.html#gitaly-and-nfs):

> From GitLab 14.0, enhancements and bug fixes for NFS for Git repositories will no longer be considered and customer technical support will be considered out of scope.

### Stage 3.5: Hands-On Experience with Gitaly

- [ ] **Completion of Stage 3.5**

> Note: This stage is optional but highly recommended for enhancing your understanding. Please contact @manuelgrabowski or @ulisesf if run into a problem setting this up.

**Objective:** This stage is designed to give you hands-on experience with Gitaly issues through a series of practical training scenarios. 

1. [ ] If you haven't done so already, take the time to get acquainted with the [GitLab Sandbox Cloud](https://handbook.gitlab.com/handbook/support/workflows/test_env/#gitlab-sandbox-cloud-for-gcp-preferred). Ensure you're capable of deploying a basic Omnibus instance.
2. [ ] Create a new environment within hackystack. After the environment is created, modify your `terraform/main.tf`. In this step, we'll deploy a single node Omnibus instance by utilizing the `omnibus_instance` module. **It's crucial to change the `source` value of the module to `gitaly-training` from its existing version.** **Please refrain from seeding the instance**.
3. [ ] Deploy your instance following the standard process. You can choose to add a license if desired.
4. [ ] Attempt to create 10 unique projects within any namespace. Record your observations and solutions to any issues encountered in a thread below.
5. [ ] Request your trainer or a Gitaly expert to evaluate your findings for constructive feedback.

Remember, the goal here is not just to complete the tasks but to understand the underlying processes and potential issues. So, take your time, be thorough, and don't hesitate to ask for guidance when needed.



### Stage 4. Demonstrate Knowledge

- [ ] **Done with Stage 4**

**Goal** Reinforce what you learned in other stages through tickets, customer calls and documentation updates

1. [ ] Answer **5** Gitaly-related tickets :tickets: and paste the links below. Collaborate with a colleague or an [expert](https://gitlab-com.gitlab.io/support/team/skills-by-subject.html) if you don't have all of the answers.
   1. Ticket:
   1. Ticket:
   1. Ticket:
   1. Ticket:
   1. Ticket:
1. [ ] Participate in two customer calls troubleshooting Gitaly.
   1. ticket for call:
   1. ticket for call:
1. [ ] Update the docs to include [add a new Praefect metric or improve the description of an existing metric](https://docs.gitlab.com/ee/administration/gitaly/monitoring.html#default-prometheus-metrics-endpoint)
1. [ ] Review the [Gitaly Documentation Needs](https://gitlab.com/groups/gitlab-org/-/epics/5075) issue and contribute if possible. Record the links to the MRs below.
1. [ ] Optional but recommended: submit docs MRs to improve the Gitaly documentation and post the links below.
   1. Docs:
   1. Docs:

### Stage 5. Final Stage

1. [ ] Have your manager review this issue.
1. [ ] Manager: Have a Gitaly expert in Support review 5-7 of the tickets from Stage 4 and report back to you on whether sufficient Gitaly knowledge has been demonstrated.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in Gitaly on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).


## :tada: Congratulations! You made it, and are well-prepared to assist customers and colleagues with Gitaly-related tickets. :tada:

If you think of any improvements to this module, please submit an MR! The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).
/label ~"Module::Gitaly"
/assign me