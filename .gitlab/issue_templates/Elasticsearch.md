---
module-name: "Elasticsearch"
area: "Core Technologies"
maintainers:
  - cleveland
---

## Overview

**Goal**: Set a clear path for Elasticsearch Expert training

**Objectives:**

- Learn about Elasticsearch
- Learn about GitLab's Elasticsearch integration
- Feel comfortable answering some common scenarios.

---

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: Elasticsearch - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Elasticsearch questions to you
1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you've started learning this knowledge area:
   
   ```
   knowledge_areas:
   - name: Elasticsearch
     level: 1
   ```

## Stage 1: Become familiar with what Elasticsearch is

1. [ ] Read Elasticsearch Documentation
    - [ ] Read [What is Elasticsearch?](https://www.elastic.co/what-is/elasticsearch)
    - [ ] Read [Elasticsearch Reference – Basic Concepts](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started-concepts.html)
    - [ ] Read [Elasticsearch Reference – Reading and Writing Documents](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-replication.html)
    - [ ] Read [Elasticsearch Reference – Analysis](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis.html)
1. [ ] Learn about the Elasticsearch Architecture
    - [ ] Watch [a video](https://www.youtube.com/watch?v=YsYUgZu9-Y4) about the basics of the Elasticsearch Architecture
1. [ ] Read the GitLab Documentation
    - [ ] Read [Elasticsearch integration](https://docs.gitlab.com/ee/integration/elasticsearch.html)
    - [ ] Read [Troubleshooting Elasticsearch](https://docs.gitlab.com/ee/administration/troubleshooting/elasticsearch.html)
    - [ ] Read [Elasticsearch lessons learnt for Advanced Global Search](https://about.gitlab.com/blog/2020/04/28/elasticsearch-update/)
1. [ ] (Optional) Consider taking the [Elasticsearch Essential Training course (2023)](https://www.linkedin.com/learning-login/share?account=2255073&forceAccount=false&redirect=https%3A%2F%2Fwww.linkedin.com%2Flearning%2Felasticsearch-essential-training-18773441%3Ftrk%3Dshare_ent_url%26shareId%3DJNxPWx2ZRcS8eR7iAnHFdw%253D%253D) with [LinkedIn Learning]( https://about.gitlab.com/handbook/people-group/learning-and-development/linkedin-learning/) (1hr 40m)


## Stage 2: Technical setup

1. [ ] Spin up an Elasticsearch installation. While you can use https://gitlabsandbox.cloud/, it is recommended to [install Elasticsearch](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html#install-elasticsearch) on your own. This can be done via VM or docker.
1. [ ] Follow the [Elasticsearch integration docs](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html)
      to integrate your Elasticsearch server with a running GitLab instance.
    - [ ] Learn how to [configure Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/settings.html) and understand what is available in `elasticsearch.yml`, `jvm.options`, and `log4j2.properties`.
1. [ ] Practice indexing and re-indexing the whole GitLab instance.
1. [ ] Practice re-indexing a specific project.
1. [ ] Feel comfortable determining which projects are not indexed.
1. [ ] Test different connection scenarios:
    - What happens when your Elasticsearch server is unavailable?
    - What happens when you configure Elasticsearch on a non-default port?
    - Can you try to perform the connection with a non-administrative user?

## Stage 3: Become familiar with the code

1. [ ] Check out some of the [Elastic code](https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee/lib/elastic/latest). 
    - [ ] Look at the [config.rb](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/elastic/latest/config.rb) to learn more about the tokenizers that we use.
    - [ ] Read about our [analyzers, tokenizers, and filters](https://docs.gitlab.com/ee/development/advanced_search.html#existing-analyzerstokenizersfilters)
1. [ ] Check out the [gitlab-elasticsearch-indexer repository](https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer/-/tree/main/) and poke through some of the code.

## Stage 4: Tickets

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

### Stage 5: Quiz

1. [ ] Contact a current Elasticsearch trainer and let them know you are ready
      for the quiz. They will provide you 3 scenarios in the comment section
      below. You will reply to each scenario as if it was a ticket. You should
      treat these as customer replies and attempt to fully resolve the
      scenario in **one** reply. Example scenarios can be found
      [here](/content/module-elasticsearch/scenarios.md). Alternatively, you can go over scenarios in a synchronous call.
1. [ ] Schedule a call with a current Elasticsearch trainer. During this call,
      you will guide them through the following:
    - [ ] Integrating a running Elasticsearch installation with GitLab. This
        does include fully indexing all your data. **Pro-tip**: Do it with
        a smaller data set to speed up the process.
    - [ ] Via the Elasticsearch API, pull the following:
      - [ ] The current health status of the Elasticsearch installation.
      - [ ] Information about the index you just created.
    - [ ] Clear the index status for one project and then re-index that project.
    - [ ] Perform a search for a basic term and verify the results are the same
        via the following methods:
      - [ ] The rails console
      - [ ] The Elasticsearch Search API
      - [ ] The GitLab Search UI
1. [ ] Once you have completed this, have the trainer comment below acknowledging your success.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

## Final stage: Completion

1. [ ] Have your manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Send a MR to declare yourself an Elasticsearch Expert on the team page
1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you're ready to work on tickets in this knowledge area:
   
   ```
   knowledge_areas:
   - name: Elasticsearch
     level: 2
   ```

/label ~module
/label ~"Module::Elasticsearch"
/assign me
